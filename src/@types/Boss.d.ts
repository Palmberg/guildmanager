type Boss = {
  number: number;
  name: string;
  id: string;
  drops: Drop[];
};
