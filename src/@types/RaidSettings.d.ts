type RaidSettings = {
  decay: number;
  bwl: number;
  mc: number;
  ony: number;
  zg: number;
};
