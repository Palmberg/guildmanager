type IncomingRaid = {
  id: string;
  date: number;
  dkpGiven: boolean;
  name: string;
  signups: Signup[];
};
