type Signup = {
  class:
    | "Druid"
    | "Hunter"
    | "Mage"
    | "Paladin"
    | "Priest"
    | "Rogue"
    | "Shaman"
    | "Warlock"
    | "Warrior";
  name: string;
  raiderId: string;
  role: "healer" | "mdps" | "rdps" | "tank";
  signupStatus: "away" | "late" | "signed";
};
