type Raid = {
  id: string;
  name: string;
  bosses: Boss[];
  imageUrl: string;
};
