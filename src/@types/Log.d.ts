type Log = {
  id: string;
  affectedPlayer: string;
  description: string;
  dkpDifference: number;
  playerId: string;
  timestamp: firebase.firestore.Timestamp;
  reverted?: boolean;
};
