type Guild = {
  name: string;
  realm: string;
  region: string;
  id: string;
  owner: string;
  raidSettings: RaidSettings;
  maintainers: string[];
  members: string[];
};
