type Raider = {
  id: string;
  name: string;
  dkp: number;
  class:
    | "Druid"
    | "Hunter"
    | "Mage"
    | "Paladin"
    | "Priest"
    | "Rogue"
    | "Shaman"
    | "Warlock"
    | "Warrior";
  role?: "healer" | "mdps" | "rdps" | "tank";
};
