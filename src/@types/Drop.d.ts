type Drop = {
  id: string;
  bis: boolean;
  dkpCost: number;
  name: string;
  imageUrl: string;
  priorities?: Array<
    | "Druid"
    | "Hunter"
    | "Mage"
    | "Paladin"
    | "Priest"
    | "Rogue"
    | "Shaman"
    | "Warlock"
    | "Warrior"
  >;
};
