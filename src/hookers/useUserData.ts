import { useEffect, useState } from "react";
import myFirebase from "../config/firebase";

const useUserData = () => {
  const [user, setUser] = useState<User | null | undefined>(undefined);

  useEffect(() => {
    const unsubscribe = myFirebase.auth().onAuthStateChanged((user) => {
      if (user) {
        setUser({
          uid: user.uid,
          email: user.email,
        });
      } else {
        setUser(null);
      }
    });

    return unsubscribe;
  }, []);

  return user;
};

export default useUserData;
