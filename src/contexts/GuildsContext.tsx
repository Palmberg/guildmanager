import React, { useState, useEffect } from "react";
import { firestore } from "firebase";

const useGuildsData = () => {
  const [guilds, setGuilds] = useState<Guild[]>([]);

  useEffect(() => {
    const unsubscribe = firestore()
      .collection("guilds/")
      .onSnapshot((querySnapshot) => {
        const guildsArray = querySnapshot.docs.map((doc) => {
          const {
            name,
            realm,
            region,
            raidSettings,
            maintainers,
            members,
            owner,
          } = doc.data();
          return {
            name,
            realm,
            region,
            id: doc.id,
            owner,
            raidSettings,
            maintainers,
            members,
          };
        });

        setGuilds(guildsArray);
      });

    return () => unsubscribe();
  }, []);

  return guilds;
};

// A context describing the current guilds.
const GuildsContext = React.createContext<Guild[]>([]);

interface Props {
  children: React.ReactNode;
}

const GuildsContextProvider = ({ children }: Props) => {
  const guilds = useGuildsData();

  return (
    <GuildsContext.Provider value={guilds}>{children}</GuildsContext.Provider>
  );
};

export { GuildsContextProvider, GuildsContext };
