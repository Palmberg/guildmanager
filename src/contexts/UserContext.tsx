import React from "react";

const defaultUser: User = {
  uid: "",
  email: "",
};

// A context describing the current user, possibly null if no user is logged in.
const UserContext = React.createContext<User>(defaultUser);

interface Props {
  children: React.ReactNode;
  userData: User;
}

const UserContextProvider = ({ children, userData }: Props) => {
  return (
    <UserContext.Provider value={userData}>{children}</UserContext.Provider>
  );
};

export { UserContextProvider, UserContext };
