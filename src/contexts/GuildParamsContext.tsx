import React, { useState, useEffect } from "react";
import { firestore } from "firebase";

const useParams = (guildData: Guild, masterLootBySignups?: Signup[]) => {
  const [raiders, setRaiders] = useState<Raider[]>([]);

  useEffect(() => {
    const unsubscribe = firestore()
      .collection(`guilds/${guildData.id}/allRaiders`)
      .doc("raiders")
      .onSnapshot((doc) => {
        const data = doc.data();
        if (data) {
          const { raiders } = data;
          const raidersParsed: Raider[] = Object.keys(raiders).map(
            (key: string) => {
              const { name, role, dkp } = raiders[`${key}`];
              const roundedDkp = Math.round(dkp * 10) / 10;
              return {
                id: key,
                class: raiders[`${key}`].class,
                name,
                role,
                dkp: roundedDkp,
              };
            }
          );
          setRaiders(raidersParsed);
        }
      });
    return () => unsubscribe();
  }, []);

  const masterLootRaiders =
    masterLootBySignups !== undefined
      ? raiders
          .slice()
          .filter(
            (raider) =>
              !!masterLootBySignups.find(
                (signup) => signup.raiderId === raider.id
              )
          )
      : raiders;

  return {
    guildData,
    raiders,
    masterLootRaiders,
  };
};

const emptyGuildObject = {
  name: "",
  realm: "",
  region: "",
  id: "",
  owner: "",
  raidSettings: {
    decay: 0,
    bwl: 0,
    mc: 0,
    ony: 0,
    zg: 0,
  },
  maintainers: [],
  members: [],
};

// A context describing the users permissions and data in current guild.
const GuildParamsContext = React.createContext<{
  permissions: "owner" | "maintainer" | "member";
  guildData: Guild;
  raiders: Raider[];
  masterLootRaiders: Raider[];
  masterLootBySignups(masterLootParams?: {
    id: string;
    signups: Signup[];
  }): void;
  masterLootId?: string;
}>({
  permissions: "member",
  guildData: emptyGuildObject,
  raiders: [],
  masterLootRaiders: [],
  masterLootBySignups: () => {},
  masterLootId: undefined,
});

interface Props {
  guildData: Guild;
  children: React.ReactNode;
  permissions: "owner" | "maintainer" | "member";
}

const GuildParamsContextProvider = ({
  guildData,
  children,
  permissions,
}: Props) => {
  const [masterLootParams, setMasterLootParams] = useState<
    { id: string; signups: Signup[] } | undefined
  >(undefined);

  const masterLootBySignups = (masterLootParams?: {
    id: string;
    signups: Signup[];
  }) => setMasterLootParams(masterLootParams);

  const params = useParams(
    guildData,
    masterLootParams !== undefined ? masterLootParams.signups : undefined
  );

  const masterLootId =
    masterLootParams !== undefined ? masterLootParams.id : undefined;

  return (
    <GuildParamsContext.Provider
      value={{ ...params, permissions, masterLootBySignups, masterLootId }}
    >
      {children}
    </GuildParamsContext.Provider>
  );
};

export { GuildParamsContextProvider, GuildParamsContext };
