import React from "react";

const emptyRaid = {
  id: "",
  imageUrl: "",
  name: "",
  bosses: [],
};

const RaidContext = React.createContext<Raid>(emptyRaid);

interface Props {
  children: React.ReactNode;
  raid: Raid;
}

const RaidContextProvider = ({ children, raid }: Props) => {
  return <RaidContext.Provider value={raid}>{children}</RaidContext.Provider>;
};

export { RaidContextProvider, RaidContext };
