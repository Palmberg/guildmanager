import React, { useContext } from "react";
import {
  Box,
  makeStyles,
  createStyles,
  Theme,
  Menu,
  MenuItem,
  Card,
  CardHeader,
  Avatar,
  Chip,
} from "@material-ui/core";
import PopupState, { bindTrigger, bindMenu } from "material-ui-popup-state";
import { firestore } from "firebase";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import dpsIcon from "../media/images/Icon-class-role-dealer-42x42.png";
import tankIcon from "../media/images/Icon-class-role-tank-42x42.png";
import healerIcon from "../media/images/Icon-class-role-healer-42x42.png";

const classColors = {
  Rogue: "#FFF569",
  Warrior: "#C79C6E",
  Hunter: "#ABD473",
  Druid: "#FF7D0A",
  Paladin: "#F58CBA",
  Warlock: "#9482C9",
  Mage: "#69CCF0",
  Priest: "#FFFFFF",
  Shaman: "#0070DE",
};

type Props = {
  removeSignup?(raider: any): void;
  raider: any;
  withMenu: boolean;
  handleSubmitSignup(
    raider: Raider,
    signupStatus: string,
    raiderClass: string,
    role: string,
    main: boolean,
    popupState: any,
    popupState2?: any,
    popupState3?: any
  ): void;
};

const RaiderSignup = (props: Props) => {
  //@ts-ignore
  const color: any = classColors[props.raider.class];

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      raider: {
        minWidth: 180,
        maxWidth: 180,
      },
      avatar: {
        border: `2px solid ${color}`,
      },
    })
  );

  const { raider, handleSubmitSignup } = props;

  const styles = useStyles();
  const { permissions, guildData } = useContext(GuildParamsContext);

  return props.withMenu ? (
    <PopupState variant="popover" popupId="demo-popup-menu">
      {(popupState) => (
        <React.Fragment>
          <Chip
            {...bindTrigger(popupState)}
            avatar={
              <Avatar
                src={
                  raider.role === "tank"
                    ? tankIcon
                    : raider.role === "healer"
                    ? healerIcon
                    : dpsIcon
                }
                className={styles.avatar}
              />
            }
            label={raider.name}
          />
          <Menu {...bindMenu(popupState)}>
            <MenuItem
              onClick={() => {
                handleSubmitSignup(
                  raider,
                  "signed",
                  raider.class,
                  raider.role,
                  true,
                  popupState
                );
              }}
            >
              Sign up
            </MenuItem>
            <MenuItem
              onClick={() => {
                handleSubmitSignup(
                  raider,
                  "late",
                  raider.class,
                  raider.role,
                  true,
                  popupState
                );
              }}
            >
              Late
            </MenuItem>
            <MenuItem
              onClick={() => {
                handleSubmitSignup(
                  raider,
                  "away",
                  raider.class,
                  raider.role,
                  true,
                  popupState
                );
              }}
            >
              Away
            </MenuItem>
            <MenuItem onClick={() => {}}>
              <PopupState variant="popover" popupId="demo-popup-menu">
                {(popupState2) => (
                  <React.Fragment>
                    <Box {...bindTrigger(popupState2)}>Play on alt</Box>
                    <Menu {...bindMenu(popupState2)}>
                      <MenuItem onClick={() => {}}>
                        <PopupState variant="popover" popupId="demo-popup-menu">
                          {(popupState3) => (
                            <React.Fragment>
                              <Box {...bindTrigger(popupState3)}>Druid</Box>
                              <Menu {...bindMenu(popupState3)}>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Druid",
                                      "mdps",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Mdps
                                </MenuItem>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Druid",
                                      "rdps",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Rdps
                                </MenuItem>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Druid",
                                      "tank",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Tank
                                </MenuItem>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Druid",
                                      "healer",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Healer
                                </MenuItem>
                              </Menu>
                            </React.Fragment>
                          )}
                        </PopupState>
                      </MenuItem>
                      <MenuItem
                        onClick={() =>
                          handleSubmitSignup(
                            raider,
                            "signed",
                            "Hunter",
                            "rdps",
                            false,
                            popupState,
                            popupState2
                          )
                        }
                      >
                        Hunter
                      </MenuItem>
                      <MenuItem
                        onClick={() =>
                          handleSubmitSignup(
                            raider,
                            "signed",
                            "Mage",
                            "rdps",
                            false,
                            popupState,
                            popupState2
                          )
                        }
                      >
                        Mage
                      </MenuItem>
                      <MenuItem onClick={() => {}}>
                        <PopupState variant="popover" popupId="demo-popup-menu">
                          {(popupState3) => (
                            <React.Fragment>
                              <Box {...bindTrigger(popupState3)}>Paladin</Box>
                              <Menu {...bindMenu(popupState3)}>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Paladin",
                                      "mdps",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Mdps
                                </MenuItem>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Paladin",
                                      "tank",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Tank
                                </MenuItem>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Paladin",
                                      "healer",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Healer
                                </MenuItem>
                              </Menu>
                            </React.Fragment>
                          )}
                        </PopupState>
                      </MenuItem>
                      <MenuItem onClick={() => {}}>
                        <PopupState variant="popover" popupId="demo-popup-menu">
                          {(popupState3) => (
                            <React.Fragment>
                              <Box {...bindTrigger(popupState3)}>Priest</Box>
                              <Menu {...bindMenu(popupState3)}>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Priest",
                                      "rdps",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Rdps
                                </MenuItem>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Priest",
                                      "healer",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Healer
                                </MenuItem>
                              </Menu>
                            </React.Fragment>
                          )}
                        </PopupState>
                      </MenuItem>
                      <MenuItem
                        onClick={() =>
                          handleSubmitSignup(
                            raider,
                            "signed",
                            "Rogue",
                            "mdps",
                            false,
                            popupState,
                            popupState2
                          )
                        }
                      >
                        Rogue
                      </MenuItem>
                      <MenuItem
                        onClick={() =>
                          handleSubmitSignup(
                            raider,
                            "signed",
                            "Warlock",
                            "rdps",
                            false,
                            popupState,
                            popupState2
                          )
                        }
                      >
                        Warlock
                      </MenuItem>
                      <MenuItem onClick={() => {}}>
                        <PopupState variant="popover" popupId="demo-popup-menu">
                          {(popupState3) => (
                            <React.Fragment>
                              <Box {...bindTrigger(popupState3)}>Warrior</Box>
                              <Menu {...bindMenu(popupState3)}>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Warrior",
                                      "rdps",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Mdps
                                </MenuItem>
                                <MenuItem
                                  onClick={() => {
                                    handleSubmitSignup(
                                      raider,
                                      "signed",
                                      "Warrior",
                                      "tank",
                                      false,
                                      popupState,
                                      popupState2,
                                      popupState3
                                    );
                                  }}
                                >
                                  Tank
                                </MenuItem>
                              </Menu>
                            </React.Fragment>
                          )}
                        </PopupState>
                      </MenuItem>
                    </Menu>
                  </React.Fragment>
                )}
              </PopupState>
            </MenuItem>
          </Menu>
        </React.Fragment>
      )}
    </PopupState>
  ) : permissions === "owner" || permissions === "maintainer" ? (
    <Chip
      avatar={
        <Avatar
          className={styles.avatar}
          src={
            raider.role === "tank"
              ? tankIcon
              : raider.role === "healer"
              ? healerIcon
              : dpsIcon
          }
        />
      }
      label={raider.name}
      onDelete={() =>
        props.removeSignup ? props.removeSignup(props.raider) : {}
      }
    />
  ) : (
    <Chip
      avatar={
        <Avatar
          className={styles.avatar}
          src={
            raider.role === "tank"
              ? tankIcon
              : raider.role === "healer"
              ? healerIcon
              : dpsIcon
          }
        />
      }
      label={raider.name}
    />
  );
};

export default RaiderSignup;
