import React, { useState, useContext, useEffect } from "react";
import { Typography, Grid, Button, Box } from "@material-ui/core";
import IncomingRaid from "./IncomingRaid";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import { firestore } from "firebase";
import { useNavigate, RouteComponentProps } from "@reach/router";
import NewRaidDialog from "../Dialogs/NewRaidDialog";

const IncomingRaidsPage: React.FC<RouteComponentProps> = () => {
  const {
    permissions,
    guildData,
    masterLootBySignups,
    masterLootId,
  } = useContext(GuildParamsContext);
  const [incomingRaids, setIncomingRaids] = useState<IncomingRaid[]>([]);
  const navigate = useNavigate();
  const [dialogOpen, setDialogOpen] = useState(false);

  useEffect(() => {
    const unsubscribe = firestore()
      .collection(`guilds/${guildData.id}/incomingRaids`)
      .orderBy("date", "desc")
      .onSnapshot((querySnapshot) => {
        const incomingRaidsArray = querySnapshot.docs.map((doc) => {
          const { date, dkpGiven, name, signups } = doc.data();
          return {
            id: doc.id,
            date: date.seconds,
            dkpGiven,
            name,
            signups,
          };
        });

        setIncomingRaids(incomingRaidsArray.filter((raid) => !raid.dkpGiven));
      });

    return () => unsubscribe();
  }, []);

  const _handleMasterLootBy = (id?: string) => {
    if (id !== undefined) {
      const incomingRaid = incomingRaids.find((raid) => raid.id === id);
      if (incomingRaid !== undefined) {
        const signups = incomingRaid.signups
          .slice()
          .filter(
            (signups) =>
              signups.signupStatus === "signed" ||
              signups.signupStatus === "late"
          );
        masterLootBySignups({ id, signups });
      } else {
        masterLootBySignups(undefined);
      }
    } else {
      masterLootBySignups(undefined);
    }
  };

  const updateContext = (id: string, signups: Signup[]) => {
    masterLootBySignups({ id, signups });
  };

  const backToGuilds = () => {
    navigate(`/guilds`);
  };

  const closeDialog = () => setDialogOpen(false);

  return permissions === null ? (
    <Grid container spacing={1} direction="column">
      <Grid item>
        <Typography>Insufficient permissions. Apply at guilds.</Typography>
      </Grid>
      <Grid item>
        <Button color="default" variant="outlined" onClick={backToGuilds}>
          Back to Guilds
        </Button>
      </Grid>
    </Grid>
  ) : (
    <Box>
      <Grid container spacing={2} direction="column">
        {incomingRaids.map((raid) => {
          return (
            <Grid item key={raid.id}>
              <IncomingRaid
                masterLootingBy={raid.id === masterLootId}
                masterLootBy={_handleMasterLootBy}
                updateContext={updateContext}
                raidInfo={raid}
              />
            </Grid>
          );
        })}
        {(permissions === "owner" || permissions === "maintainer") && (
          <Grid item>
            <Button variant="outlined" onClick={() => setDialogOpen(true)}>
              Add Raid
            </Button>
          </Grid>
        )}
      </Grid>
      {(permissions === "owner" || permissions === "maintainer") && (
        <NewRaidDialog open={dialogOpen} closeDialog={closeDialog} />
      )}
    </Box>
  );
};

export default IncomingRaidsPage;
