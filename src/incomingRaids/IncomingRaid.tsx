import React, { useState, useContext, useEffect } from "react";
import { firestore } from "firebase";
import clsx from "clsx";
import {
  Box,
  Grid,
  Card,
  CardContent,
  Typography,
  makeStyles,
  Theme,
  createStyles,
  CardHeader,
  IconButton,
  CardActions,
  Collapse,
  List,
  ListItem,
  Menu,
  MenuItem,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import lootImage from "../media/images/UI-Group-MasterLooterIcon.png";
import Img from "react-image";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import RaiderSignup from "./RaiderSignup";
import Moment from "react-moment";
import PopupState, { bindTrigger, bindMenu } from "material-ui-popup-state";
import firebase from "firebase";

type ParsedSignups = {
  Druid: Signup[];
  Hunter: Signup[];
  Mage: Signup[];
  Paladin: Signup[];
  Priest: Signup[];
  Rogue: Signup[];
  Shaman: Signup[];
  Warlock: Signup[];
  Warrior: Signup[];
};

type Props = {
  raidInfo: IncomingRaid;
  masterLootingBy: boolean;
  masterLootBy(id?: string): void;
  updateContext(id: string, signups: Signup[]): void;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    margin: {
      margin: theme.spacing(1),
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    rowEvenly: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-evenly",
      margin: theme.spacing(1),
    },
    column: {
      display: "flex",
      flexDirection: "column",
      flex: 1,
      margin: theme.spacing(1),
    },
    columnNoFlex: {
      display: "flex",
      flexDirection: "column",
      margin: theme.spacing(1),
    },
  })
);

const IncomingRaid = (props: Props) => {
  const { raidInfo } = props;
  const [collapsed, setCollapsed] = useState(true);
  const { permissions, guildData, raiders } = useContext(GuildParamsContext);

  const _handleCollapsed = () => {
    setCollapsed(!collapsed);
  };

  useEffect(() => {
    if (props.masterLootingBy) {
      props.updateContext(raidInfo.id, raidInfo.signups);
    }
  }, [raidInfo.signups]);

  const removeSignup = (raider: Signup) => {
    const ref = firestore()
      .collection(`guilds/${guildData.id}/incomingRaids`)
      .doc(props.raidInfo.id);
    ref.update({
      signups: firebase.firestore.FieldValue.arrayRemove(raider),
    });
  };

  const _renderRoles = (label: string, raiders: Array<Signup>) => {
    return raiders.length > 0 ? (
      <List>
        <ListItem>
          <Typography variant="subtitle1">
            {label} ({raiders.length})
          </Typography>
        </ListItem>
        {raiders.map((raider, index) => {
          return (
            <ListItem key={index}>
              <RaiderSignup
                raider={raider}
                removeSignup={removeSignup}
                withMenu={false}
                handleSubmitSignup={_handleSubmitSignup}
              />
            </ListItem>
          );
        })}
      </List>
    ) : undefined;
  };

  const _handleSubmitSignup = (
    raider: Raider,
    signupStatus: string,
    raiderClass: string,
    role: string,
    main: boolean,
    popupState: any,
    popupState2?: any,
    popupState3?: any
  ) => {
    const signupObject = {
      class: raiderClass,
      name: main ? raider.name : `${raider.name}Alt`,
      raiderId: raider.id,
      role,
      signupStatus,
    };
    const signupArrayRef = firestore()
      .collection(`guilds/${guildData.id}/incomingRaids`)
      .doc(raidInfo.id);
    signupArrayRef.update({
      signups: firebase.firestore.FieldValue.arrayUnion(signupObject),
    });
    popupState.close();
    popupState2 !== undefined && popupState2.close();
    popupState3 !== undefined && popupState3.close();
  };

  const renderNotAnsweredRaiders = () => {
    const notAnswered = raiders
      .slice()
      .filter(
        (raider) =>
          !props.raidInfo.signups.some(
            (element) => raider.id === element.raiderId
          )
      )
      .sort((a, b) => (a.class < b.class ? -1 : 1));
    return notAnswered.length > 0 ? (
      <Box className={styles.margin}>
        <Typography className={styles.margin} variant="h6">
          Not yet answered:
        </Typography>
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
          spacing={1}
        >
          {notAnswered.map((raider, index) => {
            return (
              <Grid item key={index}>
                <RaiderSignup
                  raider={raider}
                  key={index}
                  withMenu={
                    permissions === "owner" || permissions === "maintainer"
                  }
                  handleSubmitSignup={_handleSubmitSignup}
                />
              </Grid>
            );
          })}
        </Grid>
      </Box>
    ) : undefined;
  };

  const renderAwayRaiders = () => {
    const away = props.raidInfo.signups
      .slice()
      .filter((raider) => raider.signupStatus === "away");
    return away.length > 0 ? (
      <Box className={styles.margin}>
        <Typography className={styles.margin} variant="h6">
          Away:
        </Typography>
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
          spacing={1}
        >
          {away.map((raider, index) => {
            return (
              <Grid item key={index}>
                <RaiderSignup
                  raider={raider}
                  key={index}
                  removeSignup={removeSignup}
                  withMenu={false}
                  handleSubmitSignup={_handleSubmitSignup}
                />
              </Grid>
            );
          })}
        </Grid>
      </Box>
    ) : undefined;
  };

  const renderLateRaiders = () => {
    const late = props.raidInfo.signups
      .slice()
      .filter((raider) => raider.signupStatus === "late");
    return late.length > 0 ? (
      <Box className={styles.margin}>
        <Typography className={styles.margin} variant="h6">
          Late:
        </Typography>
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
          spacing={1}
        >
          {late.map((raider, index) => {
            return (
              <Grid item key={index}>
                <RaiderSignup
                  raider={raider}
                  key={index}
                  removeSignup={removeSignup}
                  withMenu={false}
                  handleSubmitSignup={_handleSubmitSignup}
                />
              </Grid>
            );
          })}
        </Grid>
      </Box>
    ) : undefined;
  };

  const renderSignups = () => {
    const signupsParsed: ParsedSignups = {
      Druid: [],
      Hunter: [],
      Mage: [],
      Paladin: [],
      Priest: [],
      Rogue: [],
      Shaman: [],
      Warlock: [],
      Warrior: [],
    };
    let late = 0;
    let tanks = 0;
    let healers = 0;
    let rdps = 0;
    let mdps = 0;
    raidInfo.signups.forEach((signup) => {
      if (signup.signupStatus === "signed") {
        switch (signup.role) {
          case "mdps":
            mdps += 1;
            break;
          case "rdps":
            rdps += 1;
            break;
          case "tank":
            tanks += 1;
            break;
          case "healer":
            healers += 1;
            break;
        }
        signupsParsed[signup.class].push(signup);
      } else if (signup.signupStatus === "late") {
        late += 1;
      }
    });

    return (
      <Box className={styles.margin}>
        {raidInfo.signups.find(
          (signup) => signup.signupStatus === "signed"
        ) && (
          <Typography
            className={styles.margin}
            variant="h5"
          >{`Signups: Tanks(${tanks}) Healers(${healers}) Rdps(${rdps}) Mdps(${mdps}) Total(${
            tanks + healers + rdps + mdps
          } + ${late}) `}</Typography>
        )}
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
        >
          {_renderRoles(
            "Druids",
            signupsParsed.Druid.sort((a, b) => (a.role > b.role ? -1 : 1))
          )}
          {_renderRoles("Hunters", signupsParsed.Hunter)}
          {_renderRoles("Mages", signupsParsed.Mage)}
          {_renderRoles(
            "Paladins",
            signupsParsed.Paladin.sort((a, b) => (a.role > b.role ? -1 : 1))
          )}
          {_renderRoles(
            "Priests",
            signupsParsed.Priest.sort((a, b) => (a.role > b.role ? -1 : 1))
          )}
          {_renderRoles("Rogues", signupsParsed.Rogue)}
          {_renderRoles(
            "Shamans",
            signupsParsed.Shaman.sort((a, b) => (a.role > b.role ? -1 : 1))
          )}
          {_renderRoles("Warlocks", signupsParsed.Warlock)}
          {_renderRoles(
            "Warriors",
            signupsParsed.Warrior.sort((a, b) => (a.role > b.role ? -1 : 1))
          )}
        </Grid>
      </Box>
    );
  };

  const _handleMasterLootBy = () => {
    props.masterLootingBy
      ? props.masterLootBy()
      : props.masterLootBy(props.raidInfo.id);
  };

  const _handleGiveDkp = (popupState: any) => {
    const { bwl, mc, ony, zg } = guildData.raidSettings;
    const { name } = raidInfo;
    let totalDkp = 0;
    switch (name) {
      case "Blackwing Lair":
        totalDkp = bwl;
        break;
      case "Molten Core":
        totalDkp = mc;
        break;
      case "MC+Ony":
        totalDkp = mc + ony;
        break;
      case "Zul'Gurub":
        totalDkp = zg;
        break;
      case "Onyxia":
        totalDkp = ony;
        break;
    }
    if (
      totalDkp === 0 &&
      window.confirm(
        `This raid is worth ${totalDkp} dkp, would you like to archive it?`
      )
    ) {
      firestore()
        .collection(`guilds/${guildData.id}/incomingRaids`)
        .doc(raidInfo.id)
        .update({
          dkpGiven: true,
        });
    }
    if (
      totalDkp !== 0 &&
      window.confirm(`Give ${totalDkp} dkp to all signed players?`)
    ) {
      const updatedRaidersObj: { [raiders: string]: any } = {};
      raidInfo.signups.forEach((signup) => {
        if (signup.signupStatus === "signed") {
          updatedRaidersObj[
            `raiders.${signup.raiderId}.dkp`
          ] = firebase.firestore.FieldValue.increment(totalDkp);
          firestore()
            .collection(`guilds/${guildData.id}/dkpLogs`)
            .doc()
            .set({
              affectedPlayer: signup.name,
              playerId: signup.raiderId,
              description: `Was given full dkp (${totalDkp}) for ${name}`,
              timestamp: firebase.firestore.Timestamp.now(),
              dkpDifference: totalDkp,
              reverted: false,
            });
        }
      });
      firestore()
        .collection(`guilds/${guildData.id}/allRaiders/`)
        .doc("raiders")
        .update(updatedRaidersObj)
        .then(() => {
          firestore()
            .collection(`guilds/${guildData.id}/incomingRaids`)
            .doc(raidInfo.id)
            .update({
              dkpGiven: true,
            })
            .then(() => {
              popupState.close();
            });
        });
    } else {
      popupState.close();
    }
  };

  const _handleDeleteRaid = (popupState: any) => {
    if (window.confirm(`Delete ${raidInfo.name} ${raidInfo.date}?`)) {
      firestore()
        .collection(`guilds/${guildData.id}/incomingRaids`)
        .doc(raidInfo.id)
        .delete()
        .then(() => {
          popupState.close();
        });
    } else {
      popupState.close();
    }
  };

  const _copyToNewRaid = (name: string, popupState: any, popupState2: any) => {
    firestore()
      .collection(`guilds/${guildData.id}/incomingRaids`)
      .add({
        date: new Date(),
        name,
        signups: raidInfo.signups,
        dkpGiven: false,
      })
      .then(() => {
        popupState.close();
        popupState2.close();
      });
  };

  const styles = useStyles();

  return (
    <Card>
      <CardHeader
        action={
          (permissions === "owner" || permissions === "maintainer") && (
            <PopupState variant="popover" popupId="demo-popup-menu">
              {(popupState) => (
                <React.Fragment>
                  <IconButton aria-label="delete" {...bindTrigger(popupState)}>
                    <MoreVertIcon />
                  </IconButton>
                  <Menu {...bindMenu(popupState)}>
                    <MenuItem onClick={() => _handleGiveDkp(popupState)}>
                      Give dkp
                    </MenuItem>
                    <MenuItem onClick={() => {}}>
                      <PopupState variant="popover" popupId="demo-popup-menu">
                        {(popupState2) => (
                          <React.Fragment>
                            <Typography {...bindTrigger(popupState2)}>
                              Copy Raid
                            </Typography>
                            <Menu {...bindMenu(popupState2)}>
                              <MenuItem
                                onClick={() =>
                                  _copyToNewRaid(
                                    "Blackwing Lair",
                                    popupState,
                                    popupState2
                                  )
                                }
                              >
                                Blackwing Lair
                              </MenuItem>
                              <MenuItem
                                onClick={() =>
                                  _copyToNewRaid(
                                    "Molten Core",
                                    popupState,
                                    popupState2
                                  )
                                }
                              >
                                Molten Core
                              </MenuItem>
                              <MenuItem
                                onClick={() =>
                                  _copyToNewRaid(
                                    "Onyxia",
                                    popupState,
                                    popupState2
                                  )
                                }
                              >
                                Onyxia
                              </MenuItem>
                              <MenuItem
                                onClick={() =>
                                  _copyToNewRaid(
                                    "Zul'Gurub",
                                    popupState,
                                    popupState2
                                  )
                                }
                              >
                                Zul'Gurub
                              </MenuItem>
                            </Menu>
                          </React.Fragment>
                        )}
                      </PopupState>
                    </MenuItem>
                    <MenuItem onClick={() => _handleDeleteRaid(popupState)}>
                      <Typography color="secondary">Delete Raid</Typography>
                    </MenuItem>
                  </Menu>
                </React.Fragment>
              )}
            </PopupState>
          )
        }
        title={raidInfo.name}
        subheader={
          <Moment format="D MMM YYYY" local unix>
            {raidInfo.date}
          </Moment>
        }
      />
      <CardActions disableSpacing>
        {!raidInfo.dkpGiven &&
          (permissions === "owner" || permissions === "maintainer") && (
            <IconButton
              aria-label="Master loot by"
              onClick={_handleMasterLootBy}
            >
              {props.masterLootingBy ? (
                <Img
                  src={lootImage}
                  height={24}
                  width={24}
                  style={{ borderRadius: 12 }}
                />
              ) : (
                <Img
                  src={lootImage}
                  height={24}
                  width={24}
                  style={{ borderRadius: 12, opacity: 0.3 }}
                />
              )}
            </IconButton>
          )}
        <IconButton
          className={clsx(styles.expand, {
            [styles.expandOpen]: !collapsed,
          })}
          onClick={_handleCollapsed}
          aria-expanded={!collapsed}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={!collapsed} timeout="auto" unmountOnExit>
        <CardContent>
          {renderSignups()}
          {renderNotAnsweredRaiders()}
          {renderLateRaiders()}
          {renderAwayRaiders()}
        </CardContent>
      </Collapse>
    </Card>
  );
};

export default IncomingRaid;
