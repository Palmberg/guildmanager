import React from "react";
import { CssBaseline, ThemeProvider, createMuiTheme } from "@material-ui/core";

import { UserContextProvider } from "./contexts/UserContext";
import useUserData from "./hookers/useUserData";
import { LoggedInRouter, LoggedOutRouter } from "./routers";

const theme = createMuiTheme({
  palette: {
    type: "dark",
  },
  spacing: 8,
});

const App = () => {
  const user = useUserData();

  return user === undefined ? null : (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {user !== null ? (
        <UserContextProvider userData={user}>
          <LoggedInRouter />
        </UserContextProvider>
      ) : (
        <LoggedOutRouter />
      )}
    </ThemeProvider>
  );
};

export default App;
