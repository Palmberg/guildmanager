import React, { useEffect, useContext, useState } from "react";
import { Grid } from "@material-ui/core";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import { firestore } from "firebase";
import Raid from "./Raid";
import { RaidContextProvider } from "../contexts/RaidContext";
import { RouteComponentProps } from "@reach/router";

const RaidsPage: React.FC<RouteComponentProps> = () => {
  const { guildData } = useContext(GuildParamsContext);
  const [raids, setRaids] = useState<Raid[]>([]);
  const [raidInFocus, setRaidInFocus] = useState<string | undefined>(undefined);

  useEffect(() => {
    const unsubscribe = firestore()
      .collection(`guilds/${guildData.id}/raids`)
      .onSnapshot((querySnapshot) => {
        const raidsArray = querySnapshot.docs.map((doc) => {
          const { name, bosses, imageUrl } = doc.data();
          return {
            id: doc.id,
            name,
            bosses,
            imageUrl,
          };
        });

        setRaids(raidsArray);
      });

    return () => unsubscribe();
  }, []);

  const focusRaid = (id: string) => setRaidInFocus(id);

  return raidInFocus !== undefined ? (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={12} md={12} lg={9} xl={9}>
        <RaidContextProvider
          raid={raids.find((raid) => raidInFocus === raid.id)!}
        >
          <Raid focused={true} focusRaid={focusRaid} />
        </RaidContextProvider>
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={3} xl={3}>
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="center"
          spacing={2}
        >
          {raids
            .filter((raid) => raid.id !== raidInFocus)
            .map((raid) => {
              return (
                <Grid item key={raid.id}>
                  <RaidContextProvider raid={raid}>
                    <Raid focused={false} focusRaid={focusRaid} />
                  </RaidContextProvider>
                </Grid>
              );
            })}
        </Grid>
      </Grid>
    </Grid>
  ) : (
    <Grid
      container
      direction="row"
      justify="flex-start"
      alignItems="flex-start"
      spacing={2}
    >
      {raids.map((raid) => {
        return (
          <Grid item key={raid.id}>
            <RaidContextProvider raid={raid}>
              <Raid focused={false} focusRaid={focusRaid} />
            </RaidContextProvider>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default RaidsPage;
