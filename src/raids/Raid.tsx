import React, { useContext, useState } from "react";
import {
  Card,
  makeStyles,
  Theme,
  createStyles,
  CardHeader,
  Avatar,
  IconButton,
  CardMedia,
  CardActionArea,
  Menu,
  MenuItem,
  List,
  ListItem,
  Box,
} from "@material-ui/core";
import { red } from "@material-ui/core/colors";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import { RaidContext } from "../contexts/RaidContext";
import PopupState, { bindTrigger, bindMenu } from "material-ui-popup-state";
import skullIcon from "../media/images/UI-Skull-32x32.png";
import Boss from "./Boss";
import MasterLootDialog from "../Dialogs/MasterLootDialog";
import { firestore } from "firebase";
import firebase from "firebase";

interface Props {
  focused: boolean;
  focusRaid(id: string): void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: "56.25%", // 16:9
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    avatar: {
      backgroundColor: red[500],
    },
  })
);

const Raid: React.FC<Props> = ({ focused, focusRaid }: Props) => {
  const classes = useStyles();
  const { permissions, guildData } = useContext(GuildParamsContext);
  const raid = useContext(RaidContext);
  const [masterLootingDrop, setMasterLootingDrop] = useState<Drop | undefined>(
    undefined
  );

  const onGiveItem = (drop: Drop, raider: Raider) => {
    const updatedObject: { [raiders: string]: any } = {};
    updatedObject[
      `raiders.${raider.id}.dkp`
    ] = firebase.firestore.FieldValue.increment(-drop.dkpCost);
    Promise.all([
      firestore()
        .collection(`guilds/${guildData.id}/allRaiders/`)
        .doc("raiders")
        .update(updatedObject),
      firestore()
        .collection(`guilds/${guildData.id}/dkpLogs`)
        .doc()
        .set({
          affectedPlayer: raider.name,
          playerId: raider.id,
          description: `Was given item: ${drop.name}`,
          dkpDifference: -drop.dkpCost,
          timestamp: firebase.firestore.Timestamp.now(),
          reverted: false,
        }),
    ]).then(() => {
      setMasterLootingDrop(undefined);
    });
  };

  const onMasterLoot = (drop: Drop) => setMasterLootingDrop(drop);
  const { bwl, mc, ony, zg } = guildData.raidSettings;

  return focused ? (
    <Box>
      <List>
        {raid.bosses.map((boss) => {
          return (
            <ListItem key={boss.name}>
              <Boss boss={boss} onMasterLoot={onMasterLoot} />
            </ListItem>
          );
        })}
      </List>
      {masterLootingDrop !== undefined && (
        <MasterLootDialog
          onAccept={onGiveItem}
          drop={masterLootingDrop}
          onCancel={() => setMasterLootingDrop(undefined)}
        />
      )}
    </Box>
  ) : (
    <Card className={classes.root}>
      <CardHeader
        avatar={<Avatar aria-label="recipe" src={skullIcon} />}
        action={
          (permissions === "owner" || permissions === "maintainer") && (
            <PopupState variant="popover" popupId="demo-popup-menu">
              {(popupState) => (
                <React.Fragment>
                  <IconButton
                    aria-label="delete"
                    onClick={() => {}}
                    {...bindTrigger(popupState)}
                  >
                    <MoreVertIcon />
                  </IconButton>
                  <Menu {...bindMenu(popupState)}>
                    <MenuItem onClick={() => {}}>Edit dkp reward</MenuItem>
                  </Menu>
                </React.Fragment>
              )}
            </PopupState>
          )
        }
        title={raid.name}
        subheader={`Dkp reward: ${
          raid.id === "bwl"
            ? bwl
            : raid.id === "mc"
            ? mc
            : raid.id === "ony"
            ? ony
            : raid.id === "zg"
            ? zg
            : 0
        }`}
      />
      <CardActionArea onClick={() => focusRaid(raid.id)}>
        <CardMedia className={classes.media} image={raid.imageUrl} />
      </CardActionArea>
    </Card>
  );
};

export default Raid;
