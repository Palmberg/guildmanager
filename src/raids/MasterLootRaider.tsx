import React from "react";
import {
  makeStyles,
  Theme,
  createStyles,
  Avatar,
  Typography,
  Chip,
} from "@material-ui/core";

interface Props {
  raider: Raider;
  onClick(raider: Raider): void;
}

const classColors = {
  rogue: "#FFF569",
  warrior: "#C79C6E",
  hunter: "#ABD473",
  druid: "#FF7D0A",
  paladin: "#F58CBA",
  warlock: "#9482C9",
  mage: "#69CCF0",
  priest: "#FFFFFF",
  shaman: "#0070DE",
};

const MasterLootRaider: React.FC<Props> = ({ raider, onClick }: Props) => {
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      Druid: {
        color: theme.palette.getContrastText(classColors.druid),
        backgroundColor: classColors.druid,
      },
      Hunter: {
        color: theme.palette.getContrastText(classColors.hunter),
        backgroundColor: classColors.hunter,
      },
      Mage: {
        color: theme.palette.getContrastText(classColors.mage),
        backgroundColor: classColors.mage,
      },
      Paladin: {
        color: theme.palette.getContrastText(classColors.paladin),
        backgroundColor: classColors.paladin,
      },
      Priest: {
        color: theme.palette.getContrastText(classColors.priest),
        backgroundColor: classColors.priest,
      },
      Rogue: {
        color: theme.palette.getContrastText(classColors.rogue),
        backgroundColor: classColors.rogue,
      },
      Shaman: {
        color: theme.palette.getContrastText(classColors.shaman),
        backgroundColor: classColors.shaman,
      },
      Warlock: {
        color: theme.palette.getContrastText(classColors.warlock),
        backgroundColor: classColors.warlock,
      },
      Warrior: {
        color: theme.palette.getContrastText(classColors.warrior),
        backgroundColor: classColors.warrior,
      },
    })
  );

  const styles = useStyles();

  return (
    <Chip
      onClick={() => onClick(raider)}
      avatar={
        <Avatar className={styles[raider.class]}>
          {raider.class[0].toUpperCase()}
        </Avatar>
      }
      label={`${raider.name} ${raider.dkp}`}
    />
  );
};

export default MasterLootRaider;
