import React, { useContext, useState } from "react";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import { RaidContext } from "../contexts/RaidContext";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import CloseIcon from "@material-ui/icons/Close";
import {
  Card,
  CardHeader,
  Avatar,
  IconButton,
  MenuItem,
  CardContent,
  createStyles,
  makeStyles,
  Theme,
  Typography,
  Grid,
  TextField,
  Chip,
  ListItem,
  List,
} from "@material-ui/core";
import { firestore } from "firebase";

interface Props {
  drop: Drop;
  onMasterLoot(drop: Drop): void;
}

const classColors = {
  rogue: "#FFF569",
  warrior: "#C79C6E",
  hunter: "#ABD473",
  druid: "#FF7D0A",
  paladin: "#F58CBA",
  warlock: "#9482C9",
  mage: "#69CCF0",
  priest: "#FFFFFF",
  shaman: "#0070DE",
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    Druid: {
      color: theme.palette.getContrastText(classColors.druid),
      backgroundColor: classColors.druid,
    },
    Hunter: {
      color: theme.palette.getContrastText(classColors.hunter),
      backgroundColor: classColors.hunter,
    },
    Mage: {
      color: theme.palette.getContrastText(classColors.mage),
      backgroundColor: classColors.mage,
    },
    Paladin: {
      color: theme.palette.getContrastText(classColors.paladin),
      backgroundColor: classColors.paladin,
    },
    Priest: {
      color: theme.palette.getContrastText(classColors.priest),
      backgroundColor: classColors.priest,
    },
    Rogue: {
      color: theme.palette.getContrastText(classColors.rogue),
      backgroundColor: classColors.rogue,
    },
    Shaman: {
      color: theme.palette.getContrastText(classColors.shaman),
      backgroundColor: classColors.shaman,
    },
    Warlock: {
      color: theme.palette.getContrastText(classColors.warlock),
      backgroundColor: classColors.warlock,
    },
    Warrior: {
      color: theme.palette.getContrastText(classColors.warrior),
      backgroundColor: classColors.warrior,
    },
  })
);

const Drop: React.FC<Props> = ({ drop, onMasterLoot }: Props) => {
  const { permissions, guildData } = useContext(GuildParamsContext);
  const styles = useStyles();
  const [editmode, setEditmode] = useState(false);
  const raid = useContext(RaidContext);
  const [avatarHover, setAvatarHover] = useState(false);

  const handleOnMasterLootPress = () => {
    if (editor) {
      onMasterLoot(drop);
    }
  };

  const handleEditmodePress = () => {
    setEditmode(!editmode);
  };

  const onAddPriority = (priority: string) => {
    if (
      priority === "Druid" ||
      priority === "Hunter" ||
      priority === "Mage" ||
      priority === "Paladin" ||
      priority === "Priest" ||
      priority === "Rogue" ||
      priority === "Shaman" ||
      priority === "Warlock" ||
      priority === "Warrior"
    ) {
      const slicedBosses = raid.bosses.slice();
      slicedBosses.forEach((boss) => {
        boss.drops.forEach((bossDrop) => {
          if (bossDrop.id === drop.id) {
            if (drop.priorities !== undefined) {
              drop.priorities.push(priority);
            } else {
              drop.priorities = [priority];
            }
          }
        });
      });
      firestore()
        .collection(`guilds/${guildData.id}/raids`)
        .doc(raid.id)
        .update({
          bosses: slicedBosses,
        });
    }
  };

  const onDeletePriority = (priority: string) => {
    if (
      priority === "Druid" ||
      priority === "Hunter" ||
      priority === "Mage" ||
      priority === "Paladin" ||
      priority === "Priest" ||
      priority === "Rogue" ||
      priority === "Shaman" ||
      priority === "Warlock" ||
      priority === "Warrior"
    ) {
      const slicedBosses = raid.bosses.slice();
      slicedBosses.forEach((boss) => {
        boss.drops.forEach((bossDrop) => {
          if (bossDrop.id === drop.id) {
            if (bossDrop.priorities !== undefined) {
              const priosFiltered = bossDrop.priorities
                .slice()
                .filter((prio) => prio !== priority);
              bossDrop.priorities = priosFiltered;
            }
          }
        });
      });
      firestore()
        .collection(`guilds/${guildData.id}/raids`)
        .doc(raid.id)
        .update({
          bosses: slicedBosses,
        });
    }
  };

  const updateDkp = (newPrice: string) => {
    const priceAsInt = parseInt(newPrice);
    if (priceAsInt === 0 || !!priceAsInt) {
      const slicedBosses = raid.bosses.slice();
      slicedBosses.forEach((boss) => {
        boss.drops.forEach((bossDrop) => {
          if (bossDrop.id === drop.id) {
            bossDrop.dkpCost = priceAsInt;
          }
        });
      });
      firestore()
        .collection(`guilds/${guildData.id}/raids`)
        .doc(raid.id)
        .update({
          bosses: slicedBosses,
        });
    }
  };

  const editor = permissions === "owner" || permissions === "maintainer";

  return !editmode ? (
    <Card>
      <CardHeader
        avatar={
          <Avatar
            onMouseOver={() => setAvatarHover(true)}
            onMouseOut={() => setAvatarHover(false)}
            aria-label="recipe"
            src={drop.imageUrl}
            onClick={handleOnMasterLootPress}
            style={{ opacity: editor && avatarHover ? 0.3 : 1 }}
          />
        }
        action={
          editor && (
            <IconButton aria-label="delete" onClick={handleEditmodePress}>
              <EditOutlinedIcon />
            </IconButton>
          )
        }
        title={drop.name}
        subheader={`Dkp cost: ${drop.dkpCost}`}
      />
      {drop.priorities !== undefined && drop.priorities.length > 0 && (
        <CardContent>
          <Grid
            container
            spacing={1}
            direction="row"
            justify="flex-start"
            alignItems="center"
          >
            <Grid item>
              <Typography>Priorities:</Typography>
            </Grid>
            {drop.priorities.map((priority) => {
              return (
                <Grid item>
                  <Avatar className={styles[priority]}>
                    {priority[0].toUpperCase()}
                  </Avatar>
                </Grid>
              );
            })}
          </Grid>
        </CardContent>
      )}
    </Card>
  ) : (
    <Card>
      <CardHeader
        avatar={<Avatar aria-label="recipe" src={drop.imageUrl} />}
        action={
          editor && (
            <IconButton aria-label="delete" onClick={handleEditmodePress}>
              <CloseIcon />
            </IconButton>
          )
        }
        title={drop.name}
        subheader={`Dkp cost: ${drop.dkpCost}`}
      />
      <CardContent>
        <List>
          <ListItem>
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="center"
              spacing={1}
            >
              {drop.priorities !== undefined && drop.priorities.length > 0 && (
                <Grid item>
                  <Typography>Priorities:</Typography>
                </Grid>
              )}
              {drop.priorities !== undefined &&
                drop.priorities.map((priority) => {
                  return (
                    <Grid item>
                      <Chip
                        avatar={
                          <Avatar className={styles[priority]}>
                            {priority[0].toUpperCase()}
                          </Avatar>
                        }
                        label={priority}
                        onDelete={() => onDeletePriority(priority)}
                        variant="outlined"
                      />
                    </Grid>
                  );
                })}
            </Grid>
          </ListItem>
          {(!drop.priorities || drop.priorities.length < 8) && (
            <ListItem>
              <TextField
                fullWidth
                color="secondary"
                select
                defaultValue={""}
                label="Add priority"
                onChange={(event: any) => onAddPriority(event.target.value)}
              >
                {(drop.priorities === undefined ||
                  !drop.priorities.includes("Druid")) && (
                  <MenuItem value="Druid">Druid</MenuItem>
                )}
                {(drop.priorities === undefined ||
                  !drop.priorities.includes("Hunter")) && (
                  <MenuItem value="Hunter">Hunter</MenuItem>
                )}
                {(drop.priorities === undefined ||
                  !drop.priorities.includes("Mage")) && (
                  <MenuItem value="Mage">Mage</MenuItem>
                )}
                {(drop.priorities === undefined ||
                  !drop.priorities.includes("Paladin")) && (
                  <MenuItem value="Paladin">Paladin</MenuItem>
                )}
                {(drop.priorities === undefined ||
                  !drop.priorities.includes("Priest")) && (
                  <MenuItem value="Priest">Priest</MenuItem>
                )}
                {(drop.priorities === undefined ||
                  !drop.priorities.includes("Rogue")) && (
                  <MenuItem value="Rogue">Rogue</MenuItem>
                )}
                {(drop.priorities === undefined ||
                  !drop.priorities.includes("Shaman")) && (
                  <MenuItem value="Shaman">Shaman</MenuItem>
                )}
                {(drop.priorities === undefined ||
                  !drop.priorities.includes("Warlock")) && (
                  <MenuItem value="Warlock">Warlock</MenuItem>
                )}
                {(drop.priorities === undefined ||
                  !drop.priorities.includes("Warrior")) && (
                  <MenuItem value="Warrior">Warrior</MenuItem>
                )}
              </TextField>
            </ListItem>
          )}
          <ListItem>
            <TextField
              fullWidth
              color="secondary"
              type="number"
              defaultValue={drop.dkpCost}
              label="Change price"
              onChange={(event: any) => updateDkp(event.target.value)}
            />
          </ListItem>
        </List>
      </CardContent>
    </Card>
  );
};

export default Drop;
