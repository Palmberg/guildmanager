import React from "react";
import {
  Card,
  CardActionArea,
  makeStyles,
  Theme,
  createStyles,
  CardHeader,
  Collapse,
  CardContent,
  Grid,
} from "@material-ui/core";
import Drop from "./Drop";

interface Props {
  boss: Boss;
  onMasterLoot(drop: Drop): void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
  })
);

const Boss: React.FC<Props> = ({ boss, onMasterLoot }: Props) => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.root}>
      <CardActionArea onClick={handleExpandClick}>
        <CardHeader title={boss.name} />
      </CardActionArea>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Grid container spacing={1}>
            {boss.drops.map((drop, index) => {
              return (
                <Grid item key={index}>
                  <Drop drop={drop} onMasterLoot={onMasterLoot} />
                </Grid>
              );
            })}
          </Grid>
        </CardContent>
      </Collapse>
    </Card>
  );
};

export default Boss;
