import React, { useContext, useState } from "react";
import {
  Grid,
  Card,
  CardHeader,
  CardActionArea,
  IconButton,
  Box,
} from "@material-ui/core";
import { GuildsContext } from "../contexts/GuildsContext";
import { useNavigate, RouteComponentProps } from "@reach/router";
import { UserContext } from "../contexts/UserContext";
import LockIcon from "@material-ui/icons/Lock";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import GuildAccessDialog from "../Dialogs/GuildAccessDialog";

const GuildsRenderer: React.FC<RouteComponentProps> = () => {
  const guilds = useContext(GuildsContext);
  const user = useContext(UserContext);
  const navigate = useNavigate();
  const [hover, setHover] = useState(false);
  const [guildInDialog, setGuildInDialog] = useState<Guild | undefined>(
    undefined
  );

  return (
    <Box>
      <Grid container direction="row" justify="center" alignItems="center">
        {guilds.map((guild) => {
          const canEnter =
            guild.owner === user.uid ||
            guild.maintainers.includes(user.uid) ||
            guild.members.includes(user.uid);
          return canEnter ? (
            <Grid item key={guild.id}>
              <Card>
                <CardActionArea onClick={() => navigate(`guilds/${guild.id}/`)}>
                  <CardHeader
                    title={`<${guild.name}>`}
                    subheader={`${guild.region} ${guild.realm}`}
                  />
                </CardActionArea>
              </Card>
            </Grid>
          ) : (
            <Grid item key={guild.id}>
              <Card>
                <CardHeader
                  title={`<${guild.name}>`}
                  subheader={`${guild.region} ${guild.realm}`}
                  action={
                    <IconButton
                      onMouseOver={() => setHover(true)}
                      onMouseOut={() => setHover(false)}
                      onClick={() => setGuildInDialog(guild)}
                    >
                      {hover ? <LockOpenIcon /> : <LockIcon />}
                    </IconButton>
                  }
                />
              </Card>
            </Grid>
          );
        })}
      </Grid>
      <GuildAccessDialog
        guild={guildInDialog}
        closeDialog={() => setGuildInDialog(undefined)}
      />
    </Box>
  );
};

export default GuildsRenderer;
