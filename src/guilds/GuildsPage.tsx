import React from "react";
import {
  Typography,
  ListItem,
  ListItemIcon,
  ListItemText,
  List,
  Divider,
} from "@material-ui/core";
import { RouteComponentProps, Router, Link, useMatch } from "@reach/router";
import { GuildsContextProvider } from "../contexts/GuildsContext";
import { Shell, Header, Nav, Main } from "../shell";
import { auth } from "firebase";
import {
  People as PeopleIcon,
  Home as HomeIcon,
  Settings as SettingsIcon,
  Assignment as AssignmentIcon,
  SportsEsports as RaidsIcon,
  Schedule as IncomingRaidIcon,
  ExitToApp as SignOutIcon,
} from "@material-ui/icons";

interface NavigationListItemItemProps {
  caption: string;
  icon: typeof HomeIcon;
  url: string;
}

const NavigationListItem: React.FC<NavigationListItemItemProps> = (
  props: NavigationListItemItemProps
) => {
  const match = useMatch(props.url === "/" ? props.url : `${props.url}/`);

  return (
    <ListItem button selected={!!match} component={Link} to={props.url}>
      <ListItemIcon>
        <props.icon />
      </ListItemIcon>
      <ListItemText>{props.caption}</ListItemText>
    </ListItem>
  );
};

const NavContent: React.FC<RouteComponentProps> = () => {
  const match = useMatch("guilds/:guildId/*");

  const list = !!match
    ? [
        {
          caption: "Home",
          icon: HomeIcon,
          url: "/",
        },
        {
          caption: "Raiders",
          icon: PeopleIcon,
          url: `${match.uri}`,
        },
        {
          caption: "Logs",
          icon: AssignmentIcon,
          url: `${match.uri}/logs`,
        },
        {
          caption: "Raids",
          icon: RaidsIcon,
          url: `${match.uri}/raids`,
        },
        {
          caption: "Incoming Raids",
          icon: IncomingRaidIcon,
          url: `${match.uri}/incomingRaids`,
        },
        {
          caption: "Guild Settings",
          icon: SettingsIcon,
          url: `${match.uri}/settings`,
        },
      ]
    : [];

  return (
    <List>
      {list.map((item) => {
        return (
          <NavigationListItem
            key={item.caption}
            caption={item.caption}
            icon={item.icon}
            url={item.url}
          />
        );
      })}
      <Divider />
      <ListItem key={"signOut"} button onClick={() => auth().signOut()}>
        <ListItemIcon>
          <SignOutIcon />
        </ListItemIcon>
        <ListItemText>Sign out</ListItemText>
      </ListItem>
    </List>
  );
};

interface Props extends RouteComponentProps {
  children: JSX.Element | JSX.Element[];
}

const GuildsPage: React.FC<Props> = (props: Props) => {
  return (
    <GuildsContextProvider>
      <Shell>
        <Header>
          <Typography color="inherit" variant="h6">
            Guild Manager
          </Typography>
        </Header>
        <Nav>
          <Router>
            <NavContent path="*" />
          </Router>
        </Nav>
        <Main>{props.children}</Main>
      </Shell>
    </GuildsContextProvider>
  );
};

export default GuildsPage;
