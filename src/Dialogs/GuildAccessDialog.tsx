import React, { useState, useContext } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { firestore } from "firebase";
import { UserContext } from "../contexts/UserContext";

interface Props {
  guild?: Guild;
  closeDialog(): void;
}

const GuildAccessDialog: React.FC<Props> = (props: Props) => {
  const [key, setKey] = useState("");
  const user = useContext(UserContext);

  const handleAccept = () => {
    if (props.guild !== undefined) {
      firestore()
        .collection(`joinGuildRequest`)
        .doc()
        .set({
          key,
          guildId: props.guild.id,
          userId: user.uid,
        })
        .then(() => props.closeDialog());
    }
  };

  const handleClose = () => {
    props.closeDialog();
  };

  return (
    <div>
      <Dialog
        open={props.guild !== undefined}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">{`Join guild: ${
          props.guild !== undefined ? props.guild.name : ""
        }`}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Enter Guild Code. Your Guild leaders should have provided you with
            the code for your guild. Authentication may take up to 10 seconds,
            please wait patiently.
          </DialogContentText>
          <TextField
            margin="normal"
            id="description"
            label="Guild Code"
            type="text"
            color="secondary"
            fullWidth
            value={key}
            onChange={(event: any) => setKey(event.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={handleAccept}
            color="secondary"
            disabled={key === ""}
          >
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default GuildAccessDialog;
