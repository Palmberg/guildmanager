import React, { useState, useContext } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import { firestore } from "firebase";
import firebase from "firebase";

interface Props {
  open: boolean;
  raiders?: Raider[];
  raider?: Raider;
  closeDialog(): void;
}

const EditDkpDialog: React.FC<Props> = ({
  raider,
  raiders,
  closeDialog,
  open,
}: Props) => {
  const [dkp, setDkp] = useState(0);
  const [description, setDescription] = useState("");
  const { guildData } = useContext(GuildParamsContext);

  const handleAccept = () => {
    if (raider !== undefined) {
      const dkpAsInt = parseInt(dkp.toString());
      const updatedObject: { [raiders: string]: any } = {};
      updatedObject[
        `raiders.${raider.id}.dkp`
      ] = firebase.firestore.FieldValue.increment(dkpAsInt);
      Promise.all([
        firestore()
          .collection(`guilds/${guildData.id}/allRaiders/`)
          .doc("raiders")
          .update(updatedObject),
        firestore().collection(`guilds/${guildData.id}/dkpLogs`).doc().set({
          affectedPlayer: raider.name,
          playerId: raider.id,
          description: description,
          timestamp: firebase.firestore.Timestamp.now(),
          dkpDifference: dkpAsInt,
          reverted: false,
        }),
      ]).then(() => {
        closeDialog();
      });
    } else if (raiders !== undefined) {
      const dkpAsInt = parseInt(dkp.toString());
      const updatedObject: { [raiders: string]: any } = {};
      raiders.forEach((raider) => {
        updatedObject[
          `raiders.${raider.id}.dkp`
        ] = firebase.firestore.FieldValue.increment(dkpAsInt);
        firestore().collection(`guilds/${guildData.id}/dkpLogs`).doc().set({
          affectedPlayer: raider.name,
          playerId: raider.id,
          description: description,
          timestamp: firebase.firestore.Timestamp.now(),
          dkpDifference: dkpAsInt,
          reverted: false,
        });
      });
      firestore()
        .collection(`guilds/${guildData.id}/allRaiders/`)
        .doc("raiders")
        .update(updatedObject)
        .then(() => {
          closeDialog();
        });
    }
  };

  const handleClose = () => {
    closeDialog();
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        {raider && (
          <DialogTitle id="form-dialog-title">{`Edit Dkp for ${raider.name}`}</DialogTitle>
        )}
        <DialogContent>
          {raider && (
            <DialogContentText>
              Enter a positive number to add dkp and a negative number to remove
              dkp.
            </DialogContentText>
          )}
          <TextField
            autoFocus
            margin="normal"
            id="dkp"
            label="Dkp"
            type="number"
            fullWidth
            value={dkp}
            onChange={(event: any) => setDkp(event.target.value)}
          />
          <TextField
            margin="normal"
            id="description"
            label="Description"
            type="text"
            fullWidth
            value={description}
            onChange={(event: any) => setDescription(event.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={handleAccept}
            color="secondary"
            disabled={description === "" || !dkp}
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditDkpDialog;
