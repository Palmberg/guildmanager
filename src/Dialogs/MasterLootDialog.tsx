import React, { useContext, useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  Typography,
} from "@material-ui/core";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import MasterLootRaider from "../raids/MasterLootRaider";

interface Props {
  drop?: Drop;
  onAccept(drop: Drop, raider: Raider): void;
  onCancel(): void;
}

const MasterLootDialog: React.FC<Props> = ({ drop, onAccept, onCancel }) => {
  const { masterLootRaiders } = useContext(GuildParamsContext);
  const [needingRaiders, setNeedingRaiders] = useState<Raider[]>([]);

  const handleAccept = () => {
    if (drop !== undefined && needingRaiders.length > 0) {
      onAccept(drop, needingRaiders[0]);
    }
  };

  const onRaiderNeed = (raider: Raider) => {
    const needersSliced = needingRaiders.slice();
    needersSliced.push(raider);
    needersSliced.sort((a, b) => (a.dkp > b.dkp ? -1 : 1));
    setNeedingRaiders(needersSliced);
  };

  const onRaiderRemove = (raider: Raider) => {
    const needersSliced = needingRaiders
      .slice()
      .filter((r) => r.id !== raider.id);
    setNeedingRaiders(needersSliced);
  };

  const notNeedingRaiders = masterLootRaiders
    .slice()
    .filter((raider) => !needingRaiders.includes(raider))
    .sort((a, b) => (a.class < b.class ? -1 : 1));

  return (
    <div>
      <Dialog
        open={true}
        onClose={onCancel}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Master Looter"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Select the raiders that are needing on this drop.
          </DialogContentText>
          {needingRaiders.length > 0 && (
            <Typography variant={"h5"}>
              {`Give item to ${needingRaiders[0].name}?`}
            </Typography>
          )}
          {needingRaiders.length > 0 && (
            <Grid
              container
              direction="column"
              justify="flex-start"
              alignItems="flex-start"
              spacing={1}
            >
              <Grid item>
                <Typography variant="h6">Needing raiders:</Typography>
              </Grid>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start"
                spacing={1}
              >
                {needingRaiders.map((raider) => {
                  return (
                    <Grid key={raider.id} item>
                      <MasterLootRaider
                        raider={raider}
                        onClick={() => onRaiderRemove(raider)}
                      />
                    </Grid>
                  );
                })}
              </Grid>
            </Grid>
          )}
          {notNeedingRaiders.length > 0 && (
            <Grid
              container
              direction="column"
              justify="flex-start"
              alignItems="flex-start"
              spacing={1}
            >
              <Grid item>
                <Typography variant="h6">Select needing raiders:</Typography>
              </Grid>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start"
                spacing={1}
              >
                {notNeedingRaiders.map((raider) => {
                  return (
                    <Grid key={raider.id} item>
                      <MasterLootRaider
                        raider={raider}
                        onClick={() => onRaiderNeed(raider)}
                      />
                    </Grid>
                  );
                })}
              </Grid>
            </Grid>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={onCancel} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={handleAccept}
            disabled={needingRaiders.length === 0}
            color="secondary"
          >
            Accept
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default MasterLootDialog;
