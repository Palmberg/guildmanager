import React, { useState, useContext } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { firestore } from "firebase";
import { MenuItem } from "@material-ui/core";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import { DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";

interface Props {
  closeDialog(): void;
  open: boolean;
}

const NewRaidDialog: React.FC<Props> = (props: Props) => {
  const { guildData } = useContext(GuildParamsContext);
  const [raid, setRaid] = useState("Blackwing Lair");
  const [date, setDate] = useState<MaterialUiPickersDate>(new Date());

  const handleAccept = () => {
    if (date !== null) {
      firestore()
        .collection(`guilds/${guildData.id}/incomingRaids`)
        .add({
          date,
          name: raid,
          signups: [],
          dkpGiven: false,
        })
        .then(() => {
          props.closeDialog();
        });
    }
  };

  const handleClose = () => {
    props.closeDialog();
  };

  return (
    <div>
      <Dialog
        open={props.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create Incoming Raid</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Select raid and date to create a new incoming raid.
          </DialogContentText>
          <TextField
            fullWidth
            color="secondary"
            select
            defaultValue={"Blackwing Lair"}
            label="Select Raid"
            onChange={(event: any) => setRaid(event.target.value)}
          >
            <MenuItem value="Blackwing Lair">Blackwing Lair</MenuItem>
            <MenuItem value="Molten Core">Molten Core</MenuItem>
            <MenuItem value="Onyxia">Onyxia</MenuItem>
            <MenuItem value="Zul'Gurub">Zul'Gurub</MenuItem>
          </TextField>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DateTimePicker ampm={false} value={date} onChange={setDate} />
          </MuiPickersUtilsProvider>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button onClick={handleAccept} color="secondary">
            Create Raid
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default NewRaidDialog;
