import React, { useState, useContext } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import { firestore } from "firebase";
import { v1 } from "uuid";
import { MenuItem } from "@material-ui/core";

interface Props {
  open: boolean;
  closeDialog(): void;
}

const AddRaiderDialog: React.FC<Props> = ({ open, closeDialog }: Props) => {
  const [name, setName] = useState("");
  const [wowClass, setWowClass] = useState("Druid");
  const [role, setRole] = useState("healer");
  const { guildData } = useContext(GuildParamsContext);

  const handleAccept = () => {
    const updatedObject: { [raiders: string]: any } = {};
    updatedObject[`raiders.${v1()}`] = {
      name,
      class: wowClass,
      role,
      dkp: 0,
    };
    firestore()
      .collection(`guilds/${guildData.id}/allRaiders/`)
      .doc("raiders")
      .update(updatedObject)
      .then(() => {
        closeDialog();
      });
  };

  const handleClose = () => {
    closeDialog();
  };

  const canHeal =
    wowClass === "Druid" ||
    wowClass === "Paladin" ||
    wowClass === "Priest" ||
    wowClass === "Shaman";
  const canMdps =
    wowClass === "Druid" ||
    wowClass === "Paladin" ||
    wowClass === "Rogue" ||
    wowClass === "Shaman" ||
    wowClass === "Warrior";
  const canRdps =
    wowClass === "Druid" ||
    wowClass === "Hunter" ||
    wowClass === "Mage" ||
    wowClass === "Priest" ||
    wowClass === "Shaman" ||
    wowClass === "Warlock";
  const canTank = wowClass === "Druid" || wowClass === "Warrior";

  const handleSetClass = (value: string) => {
    switch (value) {
      case "Druid":
        setWowClass("Druid");
        break;
      case "Hunter":
        setWowClass("Hunter");
        role === "rdps" || setRole("rdps");
        break;
      case "Mage":
        setWowClass("Mage");
        role === "rdps" || setRole("rdps");
        break;
      case "Paladin":
        setWowClass("Paladin");
        role === "healer" || role === "mdps" || setRole("healer");
        break;
      case "Priest":
        setWowClass("Priest");
        role === "healer" || role === "rdps" || setRole("healer");
        break;
      case "Rogue":
        setWowClass("Rogue");
        role === "mdps" || setRole("mdps");
        break;
      case "Shaman":
        setWowClass("Shaman");
        role !== "tank" || setRole("healer");
        break;
      case "Warlock":
        setWowClass("Warlock");
        role === "rdps" || setRole("rdps");
        break;
      case "Warrior":
        setWowClass("Warrior");
        role === "tank" || role === "mdps" || setRole("mdps");
        break;
    }
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Raider creation</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Enter name, class and role for the new raider.
          </DialogContentText>
          <TextField
            autoFocus
            margin="normal"
            id="name"
            label="Name"
            fullWidth
            value={name}
            onChange={(event: any) => setName(event.target.value)}
          />
          <TextField
            fullWidth
            select
            value={wowClass}
            label="Select class"
            onChange={(event: any) => handleSetClass(event.target.value)}
          >
            <MenuItem value="Druid">Druid</MenuItem>
            <MenuItem value="Hunter">Hunter</MenuItem>
            <MenuItem value="Mage">Mage</MenuItem>
            <MenuItem value="Paladin">Paladin</MenuItem>
            <MenuItem value="Priest">Priest</MenuItem>
            <MenuItem value="Rogue">Rogue</MenuItem>
            <MenuItem value="Shaman">Shaman</MenuItem>
            <MenuItem value="Warlock">Warlock</MenuItem>
            <MenuItem value="Warrior">Warrior</MenuItem>
          </TextField>
          <TextField
            fullWidth
            select
            value={role}
            label="Select role"
            onChange={(event: any) => setRole(event.target.value)}
          >
            {canHeal && <MenuItem value="healer">Healer</MenuItem>}
            {canMdps && <MenuItem value="mdps">Mdps</MenuItem>}
            {canRdps && <MenuItem value="rdps">Rdps</MenuItem>}
            {canTank && <MenuItem value="tank">Tank</MenuItem>}
          </TextField>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={handleAccept}
            color="secondary"
            disabled={name === ""}
          >
            Create Raider
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default AddRaiderDialog;
