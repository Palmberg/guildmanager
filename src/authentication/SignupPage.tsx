import React, { useState } from "react";
import { List, ListItem, TextField, Button, Link } from "@material-ui/core";
import { auth } from "firebase";
import { Link as RouterLink, RouteComponentProps } from "@reach/router";

const SignupPage: React.FC<RouteComponentProps> = () => {
  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");
  const [errorRegister, setErrorRegister] = useState("");

  const _handleSignupPress = () => {
    auth()
      .createUserWithEmailAndPassword(registerEmail, registerPassword)
      .then(() => {})
      .catch((error) => {
        setErrorRegister(error.code);
      });
  };

  const _handleRegisterEmailChange = (event: any) => {
    setErrorRegister("");
    setRegisterEmail(event.target.value);
  };

  const _handleRegisterPasswordChange = (event: any) => {
    setErrorRegister("");
    setRegisterPassword(event.target.value);
  };

  const _handleRepeatPasswordChange = (event: any) => {
    setErrorRegister("");
    setRepeatPassword(event.target.value);
  };

  return (
    <List>
      <ListItem>
        <TextField
          color="secondary"
          error={errorRegister !== ""}
          type={"text"}
          value={registerEmail}
          onChange={_handleRegisterEmailChange}
          label={"Email"}
        />
      </ListItem>
      <ListItem>
        <TextField
          color="secondary"
          error={errorRegister !== ""}
          type={"password"}
          value={registerPassword}
          onChange={_handleRegisterPasswordChange}
          label={"Password"}
        />
      </ListItem>
      <ListItem>
        <TextField
          color="secondary"
          error={errorRegister !== ""}
          helperText={errorRegister}
          type={"password"}
          value={repeatPassword}
          onChange={_handleRepeatPasswordChange}
          label={"Repeat Password"}
        />
      </ListItem>
      <ListItem>
        <Button
          variant="outlined"
          fullWidth
          disabled={
            registerPassword === "" ||
            repeatPassword === "" ||
            registerEmail === "" ||
            repeatPassword !== registerPassword
          }
          color="default"
          onClick={_handleSignupPress}
        >
          Sign up
        </Button>
      </ListItem>
      <ListItem>
        <Link component={RouterLink} to={"/login"}>
          Already have an account? Log in!
        </Link>
      </ListItem>
    </List>
  );
};

export default SignupPage;
