import React, { useState } from "react";
import { List, ListItem, TextField, Button, Link } from "@material-ui/core";
import { auth } from "firebase";
import { Link as RouterLink, RouteComponentProps } from "@reach/router";

const LoginPage: React.FC<RouteComponentProps> = () => {
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const [errorLogin, setErrorLogin] = useState("");

  const _handleLoginPress = () => {
    auth()
      .signInWithEmailAndPassword(loginEmail, loginPassword)
      .then(() => {})
      .catch((error) => {
        setErrorLogin(error.code);
      });
  };

  const _handleLoginEmailChange = (event: any) => {
    setErrorLogin("");
    setLoginEmail(event.target.value);
  };

  const _handleLoginPasswordChange = (event: any) => {
    setErrorLogin("");
    setLoginPassword(event.target.value);
  };

  return (
    <List>
      <ListItem>
        <TextField
          color="secondary"
          error={errorLogin !== ""}
          type={"text"}
          value={loginEmail}
          onChange={_handleLoginEmailChange}
          label={"Email"}
        />
      </ListItem>
      <ListItem>
        <TextField
          color="secondary"
          error={errorLogin !== ""}
          helperText={errorLogin}
          type={"password"}
          value={loginPassword}
          onChange={_handleLoginPasswordChange}
          label={"Password"}
        />
      </ListItem>
      <ListItem>
        <Button
          variant="outlined"
          fullWidth
          color="default"
          onClick={_handleLoginPress}
        >
          Login
        </Button>
      </ListItem>
      <ListItem>
        <Link component={RouterLink} to={"/signup"}>
          Do not have an account? Sign up!
        </Link>
      </ListItem>
    </List>
  );
};

export default LoginPage;
