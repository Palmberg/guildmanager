import React from "react";
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  List,
  Divider,
  Typography,
} from "@material-ui/core";
import { RouteComponentProps, useMatch, Router, Link } from "@reach/router";
import { auth } from "firebase";
import { Shell, Header, Nav, Main } from "../shell";
import {
  People as PeopleIcon,
  CreateNewFolder as CreateGuildIcon,
  Home as HomeIcon,
  Settings as SettingsIcon,
  ExitToApp as SignOutIcon,
} from "@material-ui/icons";

interface NavigationListItemItemProps {
  caption: string;
  icon: typeof HomeIcon;
  url: string;
}

const NavigationListItem: React.FC<NavigationListItemItemProps> = (
  props: NavigationListItemItemProps
) => {
  const match = useMatch(props.url === "/" ? props.url : `${props.url}/`);

  return (
    <ListItem button selected={!!match} component={Link} to={props.url}>
      <ListItemIcon>
        <props.icon />
      </ListItemIcon>
      <ListItemText>{props.caption}</ListItemText>
    </ListItem>
  );
};

const NavContent: React.FC<RouteComponentProps> = () => {
  const match = useMatch("guilds/:guildId/*");

  const list = [
    {
      caption: "Home",
      icon: HomeIcon,
      url: "/",
    },
    {
      caption: "Guilds",
      icon: PeopleIcon,
      url: "guilds",
    },
    {
      caption: "Create Guild",
      icon: CreateGuildIcon,
      url: "createguild",
    },
    {
      caption: "Settings",
      icon: SettingsIcon,
      url: "settings",
    },
  ];

  return (
    <List>
      {list.map((item) => {
        return (
          <NavigationListItem
            key={item.caption}
            caption={item.caption}
            icon={item.icon}
            url={item.url}
          />
        );
      })}
      <Divider />
      <ListItem key={"signOut"} button onClick={() => auth().signOut()}>
        <ListItemIcon>
          <SignOutIcon />
        </ListItemIcon>
        <ListItemText>Sign out</ListItemText>
      </ListItem>
    </List>
  );
};

interface Props extends RouteComponentProps {
  children: JSX.Element | JSX.Element[];
}

const HomePage: React.FC<Props> = (props: Props) => {
  return (
    <Shell>
      <Header>
        <Typography color="inherit" variant="h6">
          Guild Manager
        </Typography>
      </Header>
      <Nav>
        <Router>
          <NavContent path="*" />
        </Router>
      </Nav>
      <Main>{props.children}</Main>
    </Shell>
  );
};

export default HomePage;
