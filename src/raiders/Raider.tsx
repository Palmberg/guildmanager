import React, { useContext } from "react";
import {
  makeStyles,
  Theme,
  createStyles,
  Grid,
  Card,
  CardHeader,
  Avatar,
  Menu,
  MenuItem,
  Box,
  CardActionArea,
  Typography,
} from "@material-ui/core";
import PopupState, { bindTrigger, bindMenu } from "material-ui-popup-state";
import { firestore } from "firebase";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import firebase from "firebase";

interface Props {
  raider: Raider;
  editDkp(raider: Raider): void;
  selectForMany?(raider: Raider): void;
  selected?: boolean;
}

const classColors = {
  Rogue: "#FFF569",
  Warrior: "#C79C6E",
  Hunter: "#ABD473",
  Druid: "#FF7D0A",
  Paladin: "#F58CBA",
  Warlock: "#9482C9",
  Mage: "#69CCF0",
  Priest: "#FFFFFF",
  Shaman: "#0070DE",
};

const Raider: React.FC<Props> = ({
  raider,
  editDkp,
  selectForMany,
  selected,
}: Props) => {
  //@ts-ignore
  const color: any = classColors[raider.class];
  const { permissions, guildData } = useContext(GuildParamsContext);

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      raider: {
        minWidth: 180,
        maxWidth: 180,
      },
      raiderSelected: {
        minWidth: 180,
        maxWidth: 180,
        background: "rgba(255, 51, 51, 0.7)",
      },
      avatar: {
        background: `linear-gradient(90deg, white -120%, ${color})`,
      },
    })
  );

  const styles = useStyles();

  const _handleDeleteRaider = (popupState: any) => {
    if (
      window.confirm(`Are you sure you want to delete raider: ${raider.name}?`)
    ) {
      const updatedObject: { [raiders: string]: any } = {};
      updatedObject[
        `raiders.${raider.id}`
      ] = firebase.firestore.FieldValue.delete();
      firestore()
        .collection(`guilds/${guildData.id}/allRaiders/`)
        .doc("raiders")
        .update(updatedObject)
        .then(() => {
          popupState.close();
        });
    }
  };

  const setRole = (role: string, popupState: any, popupState2: any) => {
    const updatedObject: { [raiders: string]: any } = {};
    updatedObject[`raiders.${raider.id}.role`] = role;
    firestore()
      .collection(`guilds/${guildData.id}/allRaiders/`)
      .doc("raiders")
      .update(updatedObject);
    popupState.close();
    popupState2.close();
  };

  return (permissions === "owner" || permissions === "maintainer") &&
    selectForMany === undefined ? (
    <Grid item key={raider.id}>
      <PopupState variant="popover" popupId="demo-popup-menu">
        {(popupState) => (
          <React.Fragment>
            <Card className={styles.raider}>
              <CardActionArea {...bindTrigger(popupState)}>
                <CardHeader
                  avatar={
                    <Avatar aria-label="class" className={styles.avatar}>
                      {raider.class[0].toUpperCase()}
                    </Avatar>
                  }
                  title={raider.name}
                  subheader={raider.dkp}
                />
              </CardActionArea>
            </Card>
            <Menu {...bindMenu(popupState)}>
              {raider.role !== undefined &&
                raider.class !== "Hunter" &&
                raider.class !== "Mage" &&
                raider.class !== "Rogue" &&
                raider.class !== "Warlock" && (
                  <MenuItem onClick={() => {}}>
                    <PopupState variant="popover" popupId="demo-popup-menu">
                      {(popupState2) => (
                        <React.Fragment>
                          <Box {...bindTrigger(popupState2)}>Change Role</Box>
                          <Menu {...bindMenu(popupState2)}>
                            {raider.role !== "mdps" &&
                              (raider.class === "Druid" ||
                                raider.class === "Paladin" ||
                                raider.class === "Rogue" ||
                                raider.class === "Shaman" ||
                                raider.class === "Warrior") && (
                                <MenuItem
                                  onClick={() => {
                                    setRole("mdps", popupState, popupState2);
                                  }}
                                >
                                  Mdps
                                </MenuItem>
                              )}
                            {raider.role !== "rdps" &&
                              (raider.class === "Druid" ||
                                raider.class === "Hunter" ||
                                raider.class === "Mage" ||
                                raider.class === "Priest" ||
                                raider.class === "Shaman" ||
                                raider.class === "Warlock") && (
                                <MenuItem
                                  onClick={() => {
                                    setRole("rdps", popupState, popupState2);
                                  }}
                                >
                                  Rdps
                                </MenuItem>
                              )}
                            {raider.role !== "tank" &&
                              (raider.class === "Druid" ||
                                raider.class === "Paladin" ||
                                raider.class === "Warrior") && (
                                <MenuItem
                                  onClick={() => {
                                    setRole("tank", popupState, popupState2);
                                  }}
                                >
                                  Tank
                                </MenuItem>
                              )}
                            {raider.role !== "healer" &&
                              (raider.class === "Druid" ||
                                raider.class === "Paladin" ||
                                raider.class === "Priest" ||
                                raider.class === "Shaman") && (
                                <MenuItem
                                  onClick={() => {
                                    setRole("healer", popupState, popupState2);
                                  }}
                                >
                                  Healer
                                </MenuItem>
                              )}
                          </Menu>
                        </React.Fragment>
                      )}
                    </PopupState>
                  </MenuItem>
                )}
              {raider.role === undefined && (
                <MenuItem onClick={() => {}}>
                  <PopupState variant="popover" popupId="demo-popup-menu">
                    {(popupState2) => (
                      <React.Fragment>
                        <Box {...bindTrigger(popupState2)}>Set Role</Box>
                        <Menu {...bindMenu(popupState2)}>
                          {(raider.class === "Druid" ||
                            raider.class === "Paladin" ||
                            raider.class === "Rogue" ||
                            raider.class === "Shaman" ||
                            raider.class === "Warrior") && (
                            <MenuItem
                              onClick={() => {
                                setRole("mdps", popupState, popupState2);
                              }}
                            >
                              Mdps
                            </MenuItem>
                          )}
                          {(raider.class === "Druid" ||
                            raider.class === "Hunter" ||
                            raider.class === "Mage" ||
                            raider.class === "Priest" ||
                            raider.class === "Shaman" ||
                            raider.class === "Warlock") && (
                            <MenuItem
                              onClick={() => {
                                setRole("rdps", popupState, popupState2);
                              }}
                            >
                              Rdps
                            </MenuItem>
                          )}
                          {(raider.class === "Druid" ||
                            raider.class === "Paladin" ||
                            raider.class === "Warrior") && (
                            <MenuItem
                              onClick={() => {
                                setRole("tank", popupState, popupState2);
                              }}
                            >
                              Tank
                            </MenuItem>
                          )}
                          {(raider.class === "Druid" ||
                            raider.class === "Paladin" ||
                            raider.class === "Priest" ||
                            raider.class === "Shaman") && (
                            <MenuItem
                              onClick={() => {
                                setRole("healer", popupState, popupState2);
                              }}
                            >
                              Healer
                            </MenuItem>
                          )}
                        </Menu>
                      </React.Fragment>
                    )}
                  </PopupState>
                </MenuItem>
              )}
              <MenuItem
                onClick={() => {
                  editDkp(raider);
                  popupState.close();
                }}
              >
                Edit Dkp
              </MenuItem>
              <MenuItem onClick={() => _handleDeleteRaider(popupState)}>
                <Typography color="secondary">Delete Raider</Typography>
              </MenuItem>
            </Menu>
          </React.Fragment>
        )}
      </PopupState>
    </Grid>
  ) : (permissions === "owner" || permissions === "maintainer") &&
    selectForMany !== undefined ? (
    <Grid item key={raider.id}>
      <Card className={selected ? styles.raiderSelected : styles.raider}>
        <CardActionArea onClick={() => selectForMany(raider)}>
          <CardHeader
            avatar={
              <Avatar aria-label="class" className={styles.avatar}>
                {raider.class[0].toUpperCase()}
              </Avatar>
            }
            title={raider.name}
            subheader={raider.dkp}
          />
        </CardActionArea>
      </Card>
    </Grid>
  ) : (
    <Grid item key={raider.id}>
      <Card className={styles.raider}>
        <CardHeader
          avatar={
            <Avatar aria-label="class" className={styles.avatar}>
              {raider.class[0].toUpperCase()}
            </Avatar>
          }
          title={raider.name}
          subheader={raider.dkp}
        />
      </Card>
    </Grid>
  );
};

export default Raider;
