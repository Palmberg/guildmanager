import React, { useContext, useState } from "react";
import {
  Grid,
  Box,
  IconButton,
  Tooltip,
  TextField,
  MenuItem,
  Button,
} from "@material-ui/core";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import Raider from "./Raider";
import EditDkpDialog from "../Dialogs/EditDkpDialog";
import { RouteComponentProps } from "@reach/router";
import AddRaiderDialog from "../Dialogs/AddRaiderDialog";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";

const RaidersPage: React.FC<RouteComponentProps> = () => {
  const { raiders, permissions } = useContext(GuildParamsContext);
  const [raiderToEdit, setRaiderToEdit] = useState<Raider | undefined>(
    undefined
  );
  const [classFilter, setClassFilter] = useState<
    | "Druid"
    | "Hunter"
    | "Mage"
    | "Paladin"
    | "Priest"
    | "Rogue"
    | "Shaman"
    | "Warlock"
    | "Warrior"
    | "No filter"
  >("No filter");
  const [roleFilter, setRoleFilter] = useState<
    "healer" | "mdps" | "rdps" | "tank" | "No filter"
  >("No filter");
  const [addRaiderDialogOpen, setAddRaiderDialogOpen] = useState(false);
  const [selectingMany, setSelectingMany] = useState(false);
  const [editDkpDialoOpen, setEditDkpDialoOpen] = useState(false);
  const [selectedRaiders, setSelectedRaiders] = useState<Raider[]>([]);

  const _handleEditDkpPress = (raider: Raider) => setRaiderToEdit(raider);

  const _closeDialog = () => {
    setRaiderToEdit(undefined);
    setEditDkpDialoOpen(false);
  };

  const handleSelectRaider = (raider: Raider) => {
    const updated = selectedRaiders.slice();
    if (selectedRaiders.find((raiderObject) => raiderObject.id === raider.id)) {
      setSelectedRaiders(
        updated.filter((raiderObject) => raiderObject.id !== raider.id)
      );
    } else {
      updated.push(raider);
      setSelectedRaiders(updated);
    }
  };

  return (
    <Box>
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="center"
        spacing={1}
      >
        <Grid item>
          <TextField
            select
            value={classFilter}
            color="secondary"
            label="Class"
            onChange={(event: any) => setClassFilter(event.target.value)}
          >
            <MenuItem value="No filter">No filter</MenuItem>
            <MenuItem value="Druid">Druid</MenuItem>
            <MenuItem value="Hunter">Hunter</MenuItem>
            <MenuItem value="Mage">Mage</MenuItem>
            <MenuItem value="Paladin">Paladin</MenuItem>
            <MenuItem value="Priest">Priest</MenuItem>
            <MenuItem value="Rogue">Rogue</MenuItem>
            <MenuItem value="Shaman">Shaman</MenuItem>
            <MenuItem value="Warlock">Warlock</MenuItem>
            <MenuItem value="Warrior">Warrior</MenuItem>
          </TextField>
        </Grid>
        <Grid item>
          <TextField
            select
            value={roleFilter}
            label="Role"
            color="secondary"
            onChange={(event: any) => setRoleFilter(event.target.value)}
          >
            <MenuItem value="No filter">No filter</MenuItem>
            <MenuItem value="healer">Healer</MenuItem>
            <MenuItem value="mdps">Mdps</MenuItem>
            <MenuItem value="rdps">Rdps</MenuItem>
            <MenuItem value="tank">Tank</MenuItem>
          </TextField>
        </Grid>
        <Grid item>
          <Button
            variant="outlined"
            disabled={roleFilter === "No filter" && classFilter === "No filter"}
            onClick={() => {
              setRoleFilter("No filter");
              setClassFilter("No filter");
            }}
          >
            Clear filters
          </Button>
        </Grid>
        {(permissions === "owner" || permissions === "maintainer") && (
          <Grid item>
            <Button
              variant="outlined"
              onClick={() => setSelectingMany(!selectingMany)}
            >
              {selectingMany ? "Stop selecting" : "Select many"}
            </Button>
          </Grid>
        )}
        {(permissions === "owner" || permissions === "maintainer") &&
          selectingMany && (
            <Grid item>
              <Button
                variant="outlined"
                onClick={() => setEditDkpDialoOpen(true)}
              >
                Give dkp
              </Button>
            </Grid>
          )}
      </Grid>
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="center"
        spacing={1}
      >
        {raiders
          .filter((raider) => {
            const filterRole = roleFilter !== "No filter";
            const filterClass = classFilter !== "No filter";
            return (
              (filterRole ? raider.role === roleFilter : true) &&
              (filterClass ? raider.class === classFilter : true)
            );
          })
          .sort((a, b) => (a.dkp > b.dkp ? -1 : 1))
          .map((raider) => (
            <Raider
              key={raider.id}
              raider={raider}
              editDkp={_handleEditDkpPress}
              selectForMany={selectingMany ? handleSelectRaider : undefined}
              selected={
                !!selectedRaiders.find(
                  (raiderObject) => raider.id === raiderObject.id
                )
              }
            />
          ))}
        {(permissions === "owner" || permissions === "maintainer") && (
          <Grid item>
            <Tooltip title="Add raider" placement="top">
              <IconButton onClick={() => setAddRaiderDialogOpen(true)}>
                <AddCircleOutlineIcon />
              </IconButton>
            </Tooltip>
          </Grid>
        )}
      </Grid>
      <EditDkpDialog
        open={editDkpDialoOpen || raiderToEdit !== undefined}
        closeDialog={_closeDialog}
        raider={raiderToEdit}
        raiders={selectedRaiders.length > 0 ? selectedRaiders : undefined}
      />
      <AddRaiderDialog
        closeDialog={() => setAddRaiderDialogOpen(false)}
        open={addRaiderDialogOpen}
      />
    </Box>
  );
};

export default RaidersPage;
