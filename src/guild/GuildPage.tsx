import React, { useContext } from "react";
import { GuildsContext } from "../contexts/GuildsContext";
import { GuildParamsContextProvider } from "../contexts/GuildParamsContext";
import { RouteComponentProps, Redirect } from "@reach/router";
import GuildRouter from "./GuildRouter";
import { UserContext } from "../contexts/UserContext";

interface GuildIdProps extends RouteComponentProps {
  guildId?: string;
}

const GuildPage: React.FC<GuildIdProps> = ({ guildId }: GuildIdProps) => {
  const guilds = useContext(GuildsContext);
  const guildData = guilds.find((guild) => guild.id === guildId);
  const user = useContext(UserContext);

  let permissions:
    | "owner"
    | "maintainer"
    | "member"
    | undefined
    | null = undefined;

  if (guildData) {
    if (user.uid === guildData.owner) {
      permissions = "owner";
    } else if (guildData.maintainers.includes(user.uid)) {
      permissions = "maintainer";
    } else if (guildData.members.includes(user.uid)) {
      permissions = "member";
    } else {
      permissions = null;
    }
  }

  return permissions === undefined ? null : permissions !== null &&
    guildData !== undefined ? (
    <GuildParamsContextProvider permissions={permissions} guildData={guildData}>
      <GuildRouter />
    </GuildParamsContextProvider>
  ) : (
    <Redirect noThrow from={`/guilds/${guildId}`} to={"/"} />
  );
};

export default GuildPage;
