import React, { useContext } from "react";
import {
  Router,
  Redirect,
  RouteComponentProps,
  useMatch,
  Link,
} from "@reach/router";
import { RaidersPage } from "../raiders";
import { LogsPage } from "../logs";
import { RaidsPage } from "../raids";
import { IncomingRaidsPage } from "../incomingRaids";
import { GuildSettingsPage } from "../guildSettings";
import { Shell, Header, Nav, Main } from "../shell";
import {
  Typography,
  ListItem,
  ListItemIcon,
  ListItemText,
  List,
  Divider,
} from "@material-ui/core";
import {
  People as PeopleIcon,
  Home as HomeIcon,
  Settings as SettingsIcon,
  Assignment as AssignmentIcon,
  SportsEsports as RaidsIcon,
  Schedule as IncomingRaidIcon,
  ExitToApp as SignOutIcon,
} from "@material-ui/icons";
import { auth } from "firebase";
import { GuildParamsContext } from "../contexts/GuildParamsContext";

interface NavigationListItemItemProps {
  caption: string;
  icon: typeof HomeIcon;
  url: string;
}

const NavigationListItem: React.FC<NavigationListItemItemProps> = (
  props: NavigationListItemItemProps
) => {
  const match = useMatch(props.url === "/" ? props.url : `${props.url}/`);

  return (
    <ListItem button selected={!!match} component={Link} to={props.url}>
      <ListItemIcon>
        <props.icon />
      </ListItemIcon>
      <ListItemText>{props.caption}</ListItemText>
    </ListItem>
  );
};

interface NavContentProps extends RouteComponentProps {
  permissions: "owner" | "maintainer" | "member";
}

const NavContent: React.FC<NavContentProps> = ({
  permissions,
}: NavContentProps) => {
  const match = useMatch("/guilds/:guildId/*");

  const list = !!match
    ? [
        {
          caption: "Home",
          icon: HomeIcon,
          url: "/",
        },
        {
          caption: "Raiders",
          icon: PeopleIcon,
          url: `${match.uri}`,
        },
        {
          caption: "Logs",
          icon: AssignmentIcon,
          url: `${match.uri}/logs`,
        },
        {
          caption: "Raids",
          icon: RaidsIcon,
          url: `${match.uri}/raids`,
        },
        {
          caption: "Incoming Raids",
          icon: IncomingRaidIcon,
          url: `${match.uri}/incomingRaids`,
        },
      ]
    : [];
  if (!!match && (permissions === "owner" || permissions === "maintainer")) {
    list.push({
      caption: "Guild Settings",
      icon: SettingsIcon,
      url: `${match.uri}/settings`,
    });
  }

  return (
    <List>
      {list.map((item) => {
        return (
          <NavigationListItem
            key={item.caption}
            caption={item.caption}
            icon={item.icon}
            url={item.url}
          />
        );
      })}
      <Divider />
      <ListItem key={"signOut"} button onClick={() => auth().signOut()}>
        <ListItemIcon>
          <SignOutIcon />
        </ListItemIcon>
        <ListItemText>Sign out</ListItemText>
      </ListItem>
    </List>
  );
};

const GuildRouter: React.FC<{}> = () => {
  const { permissions, guildData } = useContext(GuildParamsContext);

  const NotFound: React.FC<RouteComponentProps> = () => (
    <Redirect noThrow to={`/guilds/${guildData.id}`} />
  );

  return (
    <Shell>
      <Header>
        <Typography color="inherit" variant="h6">
          Guild Manager
        </Typography>
      </Header>
      <Nav>
        <Router>
          <NavContent path="*" permissions={permissions} />
        </Router>
      </Nav>
      <Main>
        <Router>
          <RaidersPage path="/" />
          <LogsPage path={"/logs"} />
          <RaidsPage path="/raids" />
          <IncomingRaidsPage path="/incomingRaids" />
          {(permissions === "owner" || permissions === "maintainer") && (
            <GuildSettingsPage path="/settings" />
          )}
          <NotFound default />
        </Router>
      </Main>
    </Shell>
  );
};

export default GuildRouter;
