import {
  Box,
  Button,
  createStyles,
  makeStyles,
  MenuItem,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Theme,
} from "@material-ui/core";
import { RouteComponentProps } from "@reach/router";
import firebase, { firestore } from "firebase";
import React, { useContext, useEffect, useState } from "react";
import Moment from "react-moment";
import { GuildParamsContext } from "../contexts/GuildParamsContext";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    container: {
      maxHeight: "100%",
    },
    margin: {
      margin: theme.spacing(1),
    },
  })
);

const LogsPage: React.FC<RouteComponentProps> = () => {
  const { permissions, guildData, raiders } = useContext(GuildParamsContext);
  const [logs, setLogs] = useState<Log[]>([]);
  const classes = useStyles();
  const [filter, setFilter] = React.useState("Show all");
  const [lastVisible, setLastVisible] = useState<firestore.DocumentData>();
  const [displayShowMoreButton, setDisplayShowMoreButton] = useState(true);

  const _handleRevertLog = (log: Log) => {
    if (
      !log.reverted &&
      window.confirm(
        `Are you sure you want to revert log ${log.description} for player ${log.affectedPlayer}`
      )
    ) {
      const { affectedPlayer, dkpDifference, playerId, description, id } = log;
      const updatedObject: { [raiders: string]: any } = {};
      updatedObject[
        `raiders.${playerId}.dkp`
      ] = firebase.firestore.FieldValue.increment(-dkpDifference);
      Promise.all([
        firestore()
          .collection(`guilds/${guildData.id}/allRaiders/`)
          .doc("raiders")
          .update(updatedObject),
        firestore()
          .collection(`guilds/${guildData.id}/dkpLogs`)
          .doc()
          .set({
            affectedPlayer,
            playerId,
            description: `REVERTED ${description}`,
            timestamp: firebase.firestore.Timestamp.now(),
            dkpDifference: -dkpDifference,
            reverted: true,
          }),
        firestore()
          .collection(`guilds/${guildData.id}/dkpLogs`)
          .doc(id)
          .update({
            reverted: true,
          }),
      ]);
    }
  };

  useEffect(() => {
    firestore()
      .collection(`guilds/${guildData.id}/dkpLogs`)
      .orderBy("timestamp", "desc")
      .limit(25)
      .get()
      .then((querySnapshot) => {
        if (querySnapshot.docs.length < 25) {
          setDisplayShowMoreButton(false);
        }
        setLastVisible(querySnapshot.docs[querySnapshot.docs.length - 1]);
        const logsArray = querySnapshot.docs.map((doc) => {
          const {
            affectedPlayer,
            description,
            dkpDifference,
            playerId,
            timestamp,
            reverted,
          } = doc.data();
          return {
            id: doc.id,
            affectedPlayer,
            description,
            dkpDifference,
            playerId,
            timestamp,
            reverted,
          };
        });
        setLogs(logsArray);
      });
  }, [raiders]);

  const seeMore = () => {
    firestore()
      .collection(`guilds/${guildData.id}/dkpLogs`)
      .orderBy("timestamp", "desc")
      .startAfter(lastVisible)
      .limit(25)
      .get()
      .then((querySnapshot) => {
        if (querySnapshot.docs.length < 25) {
          setDisplayShowMoreButton(false);
        }
        setLastVisible(querySnapshot.docs[querySnapshot.docs.length - 1]);
        const logsArray = querySnapshot.docs.map((doc) => {
          const {
            affectedPlayer,
            description,
            dkpDifference,
            playerId,
            timestamp,
            reverted,
          } = doc.data();
          return {
            id: doc.id,
            affectedPlayer,
            description,
            dkpDifference,
            playerId,
            timestamp,
            reverted,
          };
        });
        const newLogs = logs.concat(logsArray);
        setLogs(newLogs);
      });
  };

  const filterByRaider = (name: string) => {
    switch (name) {
      case "Show all":
        setDisplayShowMoreButton(true);
        firestore()
          .collection(`guilds/${guildData.id}/dkpLogs`)
          .orderBy("timestamp", "desc")
          .limit(25)
          .get()
          .then((querySnapshot) => {
            if (querySnapshot.docs.length < 25) {
              setDisplayShowMoreButton(false);
            }
            setLastVisible(querySnapshot.docs[querySnapshot.docs.length - 1]);
            const logsArray = querySnapshot.docs.map((doc) => {
              const {
                affectedPlayer,
                description,
                dkpDifference,
                playerId,
                timestamp,
                reverted,
              } = doc.data();
              return {
                id: doc.id,
                affectedPlayer,
                description,
                dkpDifference,
                playerId,
                timestamp,
                reverted,
              };
            });
            setLogs(logsArray);
          });
        setFilter("Show all");
        break;
      default:
        firestore()
          .collection(`guilds/${guildData.id}/dkpLogs`)
          .orderBy("timestamp", "desc")
          .where("affectedPlayer", "==", name)
          .limit(25)
          .get()
          .then((querySnapshot) => {
            setDisplayShowMoreButton(false);
            const logsArray = querySnapshot.docs.map((doc) => {
              const {
                affectedPlayer,
                description,
                dkpDifference,
                playerId,
                timestamp,
                reverted,
              } = doc.data();
              return {
                id: doc.id,
                affectedPlayer,
                description,
                dkpDifference,
                playerId,
                timestamp,
                reverted,
              };
            });
            setLogs(logsArray);
          });
        setFilter(name);
    }
  };

  const columns = [
    { id: "timestamp", label: "Date" },
    { id: "affectedPlayer", label: "Affected Player" },
    { id: "description", label: "Description" },
    { id: "dkpDifference", label: "Dkp Difference" },
  ];

  return (
    <Box>
      <TextField
        fullWidth
        select
        value={filter}
        color="secondary"
        label="Filter logs by raider"
        onChange={(event: any) => filterByRaider(event.target.value)}
      >
        <MenuItem value="Show all">Show all</MenuItem>
        {raiders
          .sort((a, b) => (a.name < b.name ? -1 : 1))
          .map((raider) => (
            <MenuItem key={raider.id} value={raider.name}>
              {raider.name}
            </MenuItem>
          ))}
      </TextField>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell key={column.id}>{column.label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {logs.map((log) => {
                return (
                  <TableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={log.id}
                    onClick={() =>
                      permissions === "owner" || permissions === "maintainer"
                        ? _handleRevertLog(log)
                        : {}
                    }
                  >
                    {columns.map((column) => {
                      const data =
                        column.id !== "timestamp" ? (
                          //@ts-ignore
                          log[column.id]
                        ) : (
                          <Moment format="D MMM YYYY" local unix>
                            {log[column.id].seconds}
                          </Moment>
                        );
                      return <TableCell key={column.id}>{data}</TableCell>;
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        {displayShowMoreButton && (
          <Button variant="text" onClick={seeMore}>
            + Show more
          </Button>
        )}
      </Paper>
    </Box>
  );
};

export default LogsPage;
