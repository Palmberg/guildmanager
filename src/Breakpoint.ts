import createBreakpoints, {
  keys,
} from "@material-ui/core/styles/createBreakpoints";

type Breakpoint = typeof keys[number];

export const breakpoints = keys as readonly Breakpoint[];

const { values } = createBreakpoints({});

export const breakpointValues = values as { [key in Breakpoint]: number };

export const breakpointsDownToExcluding = (breakpoint: Breakpoint) =>
  breakpoints.filter((_, index) => index > breakpoints.indexOf(breakpoint));

export const breakpointsUpToIncluding = (breakpoint: Breakpoint) =>
  breakpoints.filter((_, index) => index <= breakpoints.indexOf(breakpoint));
