// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import "firebase/analytics";

// Add the Firebase products that you want to use
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyB1lk_0DifQ9kdBp84tNhmQJjqfMKzlGSo",
  authDomain: "guildmanager-9cc46.firebaseapp.com",
  databaseURL: "https://guildmanager-9cc46.firebaseio.com",
  projectId: "guildmanager-9cc46",
  storageBucket: "guildmanager-9cc46.appspot.com",
  messagingSenderId: "932116832978",
  appId: "1:932116832978:web:420f1feeda87be2b20030b",
  measurementId: "G-LPPTWKCPMS",
};

firebase.initializeApp(firebaseConfig);

export default firebase;
