import React, { useContext } from "react";
import { Button } from "@material-ui/core";
import { RouteComponentProps } from "@reach/router";
import { GuildParamsContext } from "../contexts/GuildParamsContext";
import { firestore } from "firebase";
import firebase from "firebase";

const GuildSettingsPage: React.FC<RouteComponentProps> = () => {
  const { guildData, raiders } = useContext(GuildParamsContext);

  const handleWeeklyDecay = () => {
    if (
      window.confirm(
        `Perform weekly decay? This will remove ${guildData.raidSettings.decay}% of all raiders dkp.`
      )
    ) {
      const updatedRaidersObj: { [raiders: string]: any } = {};
      raiders.forEach((raider) => {
        const { dkp, id, name } = raider;
        const decrement =
          dkp * ((100 - guildData.raidSettings.decay) / 100) - dkp;
        updatedRaidersObj[
          `raiders.${id}.dkp`
        ] = firebase.firestore.FieldValue.increment(decrement);
        firestore()
          .collection(`guilds/${guildData.id}/dkpLogs`)
          .doc()
          .set({
            affectedPlayer: name,
            playerId: id,
            description: `Weekly decay, ${guildData.raidSettings.decay}% of dkp.`,
            timestamp: firebase.firestore.Timestamp.now(),
            dkpDifference: Math.round(decrement * 10) / 10,
            reverted: false,
          });
      });
      firestore()
        .collection(`guilds/${guildData.id}/allRaiders/`)
        .doc("raiders")
        .update(updatedRaidersObj);
    }
  };

  return (
    <Button variant="outlined" onClick={handleWeeklyDecay}>
      Weekly decay
    </Button>
  );
};

export default GuildSettingsPage;
