import React, { useState } from "react";
import { useMediaQuery, useTheme } from "@material-ui/core";
import Header from "./Header";
import LayoutContext, { defaultValue } from "./LayoutContext";
import Main from "./Main";
import Nav from "./Nav";

interface Props {
  children?: React.ReactNode;
  compactThreshold?: Breakpoint;
}

export const Shell = ({ children, compactThreshold }: Props) => {
  const theme = useTheme();

  const threshold = compactThreshold || "sm";
  const isCompact = useMediaQuery(theme.breakpoints.down(threshold));

  const [open, setOpen] = useState(false);
  const toggleNavOpen = () => setOpen(!open);
  const navOpen = open || !isCompact;

  const [collapsed, setCollapsed] = useState(false);
  const toggleNavCollapsed = () => setCollapsed(!collapsed);
  const navCollapsed = collapsed && !isCompact;

  const navWidth = navCollapsed
    ? theme.spacing(7) + 1
    : defaultValue.navFullWidth;

  return (
    <LayoutContext.Provider
      value={{
        ...defaultValue,
        navCollapsed,
        navOpen,
        navVariant: isCompact ? "temporary" : "permanent",
        navWidth: navOpen ? navWidth : defaultValue.navWidth,
        toggleNavCollapsed,
        toggleNavOpen,
      }}
    >
      {children}
    </LayoutContext.Provider>
  );
};

export { Header, Main, Nav };
