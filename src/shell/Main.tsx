import React, { useContext } from 'react';
import { Container, makeStyles } from '@material-ui/core';
import LayoutContext from './LayoutContext';

interface StyleProps {
  navCollapsed: boolean;
  navWidth: number;
}

const useStyles = makeStyles((theme) => ({
  main: ({ navCollapsed, navWidth }: StyleProps) => ({
    width: `calc(100% - ${navWidth}px)`,
    marginLeft: navWidth,
    transition: theme.transitions.create(['margin-left', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: navCollapsed
        ? theme.transitions.duration.leavingScreen
        : theme.transitions.duration.enteringScreen,
    }),
  }),
  toolbar: theme.mixins.toolbar,
}));

interface Props {
  children?: React.ReactNode;
}

const Main = ({ children }: Props) => {
  const { navCollapsed, navVariant, navWidth } = useContext(LayoutContext);
  const classes = useStyles({
    navCollapsed,
    navWidth: navVariant !== 'permanent' ? 0 : navWidth,
  });
  return (
    <main className={classes.main}>
      <div className={classes.toolbar} />
      <Container>{children}</Container>
    </main>
  );
};

export default Main;
