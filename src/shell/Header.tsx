import React, { useContext } from "react";
import {
  AppBar,
  IconButton,
  Slide,
  Toolbar,
  makeStyles,
  useScrollTrigger,
} from "@material-ui/core";
import { Menu as MenuIcon } from "@material-ui/icons";
import LayoutContext from "./LayoutContext";

interface StyleProps {
  navCollapsed: boolean;
  navWidth: number;
}

const useStyles = makeStyles((theme) => ({
  appBar: ({ navCollapsed, navWidth }: StyleProps) => ({
    width: `calc(100% - ${navWidth}px)`,
    marginLeft: navWidth,
    transition: [
      theme.transitions.create(["margin-left", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: navCollapsed
          ? theme.transitions.duration.leavingScreen
          : theme.transitions.duration.enteringScreen,
      }),
      theme.transitions.create("box-shadow"),
    ].join(","),
    backgroundColor: theme.palette.background.default,
    color: theme.palette.getContrastText(theme.palette.background.default),
  }),
  menuButton: {
    marginRight: theme.spacing(2),
  },
}));

interface Props {
  children?: React.ReactNode;
}

const Header = ({ children }: Props) => {
  const { navCollapsed, navVariant, navWidth, toggleNavOpen } = useContext(
    LayoutContext
  );
  const classes = useStyles({
    navCollapsed,
    navWidth: navVariant !== "permanent" ? 0 : navWidth,
  });
  const elevateTrigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  });
  const hideTrigger = useScrollTrigger();
  return (
    <Slide appear={false} direction="down" in={!hideTrigger}>
      <AppBar className={classes.appBar} elevation={elevateTrigger ? 4 : 0}>
        <Toolbar>
          {navVariant !== "permanent" && (
            <IconButton
              aria-label={"open menu"}
              color="inherit"
              edge="start"
              className={classes.menuButton}
              onClick={toggleNavOpen}
            >
              <MenuIcon />
            </IconButton>
          )}
          {children}
        </Toolbar>
      </AppBar>
    </Slide>
  );
};

export default Header;
