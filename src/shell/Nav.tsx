import React, { useContext } from 'react';
import { makeStyles, Drawer, IconButton, Divider } from '@material-ui/core';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import LayoutContext from './LayoutContext';

interface StyleProps {
  navCollapsed: boolean;
  navFullWidth: number;
  navWidth: number;
}

const useStyles = makeStyles((theme) => ({
  nav: ({ navWidth }: StyleProps) => ({
    width: navWidth,
    display: 'flex',
  }),
  children: ({ navFullWidth }: StyleProps) => ({
    width: navFullWidth,
    flex: '1 1 auto',
    overflowY: 'auto',
  }),
  collapse: {
    marginLeft: 'auto',
    marginRight: theme.spacing(0.5),
  },
  drawerPaper: ({ navCollapsed, navWidth }: StyleProps) => ({
    width: navWidth,
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: navCollapsed
        ? theme.transitions.duration.leavingScreen
        : theme.transitions.duration.enteringScreen,
    }),
    color: fade(theme.palette.primary.contrastText, 0.85),
    '& div[class*="MuiListItemIcon-root"]': {
      color: 'inherit',
    },
    '& button:hover, [class*="MuiListItem-button"]:hover': {
      backgroundColor: 'rgba(255, 255, 255, 0.08)',
    },
    '& [class*="Mui-selected"]': {
      backgroundColor: 'transparent',
      color: theme.palette.secondary.main,
    },
  }),
}));

interface Props {
  children?: React.ReactNode;
}

const Nav = ({ children }: Props) => {
  const {
    navCollapsed,
    navFullWidth,
    navOpen,
    navVariant,
    navWidth,
    toggleNavCollapsed,
    toggleNavOpen,
  } = useContext(LayoutContext);
  const classes = useStyles({ navCollapsed, navFullWidth, navWidth });
  
  return (
    <nav className={classes.nav}>
      <Drawer
        classes={{ paper: classes.drawerPaper }}
        variant={navVariant}
        open={navOpen}
        data-testid={`${navVariant}-drawer`}
        onClose={toggleNavOpen}
      >
        <div className={classes.children}>{children}</div>
        {navVariant === 'permanent' && (
          <>
            <Divider />
            <IconButton
              aria-label={`${navCollapsed ? 'expand' : 'collapse'} menu`}
              className={classes.collapse}
              color="inherit"
              onClick={toggleNavCollapsed}
            >
              {navCollapsed ? <ChevronRight /> : <ChevronLeft />}
            </IconButton>
          </>
        )}
      </Drawer>
    </nav>
  );
};

export default Nav;
