import React from 'react';

interface LayoutContextValue {
  navCollapsed: boolean;
  navFullWidth: number;
  navOpen: boolean;
  navWidth: number;
  navVariant: 'temporary' | 'permanent';
  toggleNavCollapsed: () => void;
  toggleNavOpen: () => void;
}

export const defaultValue: LayoutContextValue = {
  navCollapsed: false,
  navFullWidth: 240,
  navOpen: false,
  navVariant: 'permanent',
  navWidth: 0,
  toggleNavCollapsed: () => {},
  toggleNavOpen: () => {},
};

const LayoutContext = React.createContext(defaultValue);

export default LayoutContext;
