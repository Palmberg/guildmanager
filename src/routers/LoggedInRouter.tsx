import React from "react";
import { LandingPage } from "../landing";
import GuildsRenderer from "../guilds/GuildsRenderer";
import { GuildPage } from "../guild";
import { CreateGuildPage } from "../createGuild";
import { SettingsPage } from "../settings";
import { Router, Redirect, RouteComponentProps } from "@reach/router";
import { HomePage } from "../home";
import { GuildsContextProvider } from "../contexts/GuildsContext";

const NotFound: React.FC<RouteComponentProps> = () => (
  <Redirect noThrow to={"/"} />
);

const LoggedInRouter: React.FC<{}> = () => {
  return (
    <GuildsContextProvider>
      <Router>
        <HomePage path="/">
          <LandingPage path="/" />
          <CreateGuildPage path="createguild" />
          <SettingsPage path="settings" />
          <GuildsRenderer path="guilds" />
          <NotFound default />
        </HomePage>
        <GuildPage path="/guilds/:guildId/*" />
      </Router>
    </GuildsContextProvider>
  );
};

export default LoggedInRouter;
