import React from "react";
import { Router, Redirect, RouteComponentProps } from "@reach/router";
import LoginPage from "../authentication/LoginPage";
import SignupPage from "../authentication/SignupPage";
import { withTheme, Box } from "@material-ui/core";

const NotFound: React.FC<RouteComponentProps> = () => (
  <Redirect noThrow to={"/login"} />
);

const LoggedOutRouter: React.FC<{}> = () => {
  return (
    <Box display="flex" alignItems="center" justifyContent="center">
      <Router>
        <LoginPage path="login" />
        <SignupPage path="signup" />
        <NotFound default />
      </Router>
    </Box>
  );
};

export default withTheme(LoggedOutRouter);
