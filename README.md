# guildManager

- deployed at: https://guildmanager-9cc46.firebaseapp.com/
- työaikakirjanpito: https://gitlab.com/Palmberg/guildmanager/-/blob/master/ty%C3%B6aikakirjanpito%20-%20Rasmus%20Palmberg%20-%20Taulukko1.pdf

## Terms

- dkp: point system which can be used to buy items.
- guild: a collection of people.
- raider: a member of a guild.
- drop/item: gear piece which are bought using dkp.
- boss: tough encounter which needs about 40 raiders to defeat, which drop random items.
- raid: collection of bosses.
- master looter: Person in charge of contributing loot fairly.

## instructions:

Please do not remove any existing raiders, neither change their role or dkp.

### Maintainer

- Log in using the maintainer account given in the email.
- Go to Guilds tab in the navigation drawer and select Ascension.
- This is the main area where the user is located, there are 5 tabs that a maintainer can use. Raiders, Logs, Raids, Incoming Raids and Guild Settings.
- In the Raiders tab the maintainer can maintain raiders. A raider can be added using the Plus button which occurs last in the grid, do that. The data you enter does not matter. Name, Class and Role is required.
- When a raider is created it appears in the grid with 0 Dkp.
- Edit the recently added raider by pressing it. Give him 50 dkp, add a arbitrary description.
- The raider moves up in the grid when his dkp increases.
- Go to the Logs page, there you can see that your created raider has appeared with a log for the editing of dkp.
- Press the recently appeared Log and press ok for reverting the dkp given.
- Go back to Raiders and check that your created raider is back at 0 dkp.
- Check the logs that it has created a log for reverting the given dkp. This log can not be reverted, neither the previously reverted one.
- Guild Settings page there is only a Weekly decay button, by using that, every raider loses 20% of their dkp, this is for preventing hoarding.
- In Incoming raids you can see a incoming raid for 16.07.2020. That is an example for how a raid sign up looks like. Leave this be.
- Add a raid, for example Blackwing Lair for any date.
- Add your recently created raider to this raid by pressing the chip for that raider and selecting sign up. Sign up at least two raiders for this raid. You can also add a couple of more if you'd like to.
- Select the coin pouch under the title of the card (Why will be explained later) and navigate to Raids and select Blackwing Lair.
- When a raid is selected, every boss for that raid is shown with a drop down menu for the items for that boss.
- Select Ebonroc
- This view shows all the items for Ebonroc with respective prices and prioritized classes (Drake fang talisman is the only item with prio).
- An item can be edited by pressing the 'pen' next to it. A maintainer can edit the price of specific items and if needed also add prioritized classes (There is apparently a bug where a prioritized classes occurs multiple times if an item drops on more than one boss).
- Maintainers can be master looters. Press an item icon. Here the coin pouch selecting filters the list of raiders to only contain the raiders that had signed up for this raid to make it shorter and more manageable.
- In this dialog the master looter can select the players that in game choose the spend dkp for receiving this item. Select at least 2 raiders from the list.
- The selected raiders will be sorted by dkp. The dialog displays who should receive the item based on whom has the most dkp. Press Accept.
- Go to Logs, here you can now see that the raider you gave the item too has a log for when he received the item and how much it cost.
- Last big feature is for rewarding dkp for raid attendance. Go to incoming raids.
- Press the 3 dots in the top right of the recently created raid and press 'Give dkp'. Select ok.
- You can now navigate to Raiders / Logs to see that your created raider has received the dkp reward for this raid.

### Member

- Log out.
- Navigate to the sign up page and create an account, the email can be whatever with a correct email format (`x@y.z`), no email verification implemented.
- Go to guilds, Ascension is now locked. Enter by using the passphrase given in the email (CaseSensitive). This fires a cloud function which might take few seconds. It is quite slow because it has to cold boot when it is rarely used to save resources.
- A member sees about the same things as the Maintainer but with a lot more restrictions in both the UI and In firebase rules.
- You can freely navigate around the App, the members view is mainly used for following up item prices, checking when the next incoming raid is and following up in the logs what has happened to your personal dkp.
