const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

const db = admin.firestore();

exports.joinGuildRequest = functions.region('europe-west1').firestore
  .document('joinGuildRequest/{requestId}')
  .onCreate((snap, context) => {
    
    const requestData = snap.data()

    const db = admin.firestore();

    return db.doc(`guilds/${requestData.guildId}/keys/member`).get().then(querySnaphot => {
      if (querySnaphot.data().key === requestData.key) {
        const guildRef = db.doc(`guilds/${requestData.guildId}`)
        return guildRef.get().then(snap => {
          const membersSliced = snap.data().members.slice()
          membersSliced.push(requestData.userId)
          return guildRef.update({
            members: membersSliced
          }, { approved: true })
        })
      } else {
        return { approved: false }
      }
    })
      
    // perform desired operations ...
  });